//
//  model.swift
//  CryptoKitty
//
//  Created by Денис on 12.12.2017.
//  Copyright © 2017 Denis. All rights reserved.
//

class AllKitties: Decodable {
    var total: Int?
    var kitties: [Kitties]?
    var offset: Int?
    var limit: Int?
}

class Kitties: Decodable {
    var id: Int?
    var name: String?
    var image_url: String?
    var generation: Int?
    var created_at: String?
    var color: String?
    var is_fancy: Bool?
    var is_exclusive: Bool?
    var fancy_type: String?
    var status: Status?
    var owner: Owner?
    var siri: Siri?
    var matron: Matron?
}

class Status: Decodable {
    var cooldown: Int?
    var cooldown_index: Int?
    var is_ready: Bool?
    var is_gestating: Bool?
}

struct Owner: Decodable {
    var address: String?
    var nickname: String?
    var image: String?
}

class Siri: Decodable {
    var id: Int?
    var name: String?
    var image_url: String?
    var generation: Int?
    var created_at: String?
    var color: String?
    var is_fancy: Bool?
    var is_exclusive: Bool?
    var fancy_type: String?
    var owner_wallet_address: String?
    var status: StatusMatron?
}

class Matron: Decodable {
    var id: Int?
    var name: String?
    var image_url: String?
    var generation: Int?
    var created_at: String?
    var color: String?
    var is_fancy: Bool?
    var is_exclusive: Bool?
    var fancy_type: String?
    var owner_wallet_address: String?
    var status: StatusMatron?
}

class StatusMatron: Decodable {
    var isReady: Bool?
    var isGestating: Bool?
    var cooldown: Int?
}


