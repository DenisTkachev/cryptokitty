//
//  Kitten.swift
//  CryptoKitty
//
//  Created by Денис on 16.12.2017.
//  Copyright © 2017 Denis. All rights reserved.
//

class Kitten: Decodable {
    var id: Int?
    var name: String?
    var generation: Int?
    var created_at: String?
    var image_url: String?
    var color: String?
    var bio: String?
    var is_fancy: Bool?
    var is_exclusive: Bool?
    var fancy_type: String?
    var status: Status?
    var owner: Owner?
    var siri: Siri?
    var matron: Matron?
    var auction: AuctionKitten?
    var children: Children?
    var cattributes: Cattributes?
}

class Children: Decodable {
    var id: Int?
    var name: String?
    var image_url: String?
    var generation: Int?
    var created_at: String?
    var color: String?
    var is_fancy: Bool?
    var is_exclusive: Bool?
    var fancy_type: String?
    var owner_wallet_address: String?
    var status: StatusMatron?
}

class AuctionKitten: Decodable {
    
    var id: Int?
    var type: String?
    var start_price: Int?
    var end_price: Int?
    var start_time: String?
    var end_time: String?
    var current_price: Int?
    var duration: Int?
    var status: String?
    var seller: SellerKiten?
}

class SellerKiten: Decodable {
    var address: String?
    var nickname: String?
    var image: Int?
}






