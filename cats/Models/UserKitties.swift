//
//  UserKitties.swift
//  CryptoKitty
//
//  Created by Денис on 16.12.2017.
//  Copyright © 2017 Denis. All rights reserved.
//

struct UserKitties: Decodable {
    var total: Int
    var kitties: [KittiesUserInfo]
    var offset: Int
    var limit: Int
}

struct KittiesUserInfo: Decodable {
    var id: Int
    var name: String?
    var image_url: String
    var generation: Int
    var created_at: String
    var color: String
    var is_fancy: Bool
    var is_exclusive: Bool
    var fancy_type: String?
    var status: Status
    var owner: Owner
    var siri: Siri?
    var matron: Matron
}
