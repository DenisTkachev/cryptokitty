//
//  FilterSettings.swift
//  cats
//
//  Created by Денис on 17.03.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

struct FilterSettings {
    var sortBy: String = ""
    var sortDirection: Bool = true
    var is_fancy: Bool = false
    var is_exclusive: Bool = false
    var coolDown: String = ""
    
}





