//
//  AllAuctions.swift
//  CryptoKitty
//
//  Created by Денис on 16.12.2017.
//  Copyright © 2017 Denis. All rights reserved.
//

class AllAuctions: Decodable {
    var total: Int?
    var auctions: [Auctions]?
    var auctionsFiltered: [Auctions] {
        get {
            return self.applyUserFilter(auctions!)
        }
    }

    var offset: Int?
    var limit: Int?
    
    
    // sortBy: generation, cooldown, like, price
    // hight to low, low to hight
    // Cooldown:
    //    0:Fast
    //    1:Swift
    //    2:Snappy
    //    3:Brisk
    //    4:Plodding
    //    5:Slow
    //    6:Sluggish
    //    7:Catatonic
    // is_fancy
    // is_exlusive
}

class Auctions: Decodable {
    var seller: Seller?
    var kitty: Kitty?
    var start_price: String?
    var end_price: String?
    var start_time: String?
    var end_time: String?
    var current_price: String?
    var duration: String?
    var status: String?
    var type: String?
}

class Seller: Decodable {
    var address: String?
    var nickname: String?
    var image: String?
}

class Kitty: Decodable {
    var id: Int?
    var name: String?
    var image_url: String?
    var generation: Int?
    var created_at: String?
    var color: String?
    var is_fancy: Bool?
    var is_exclusive: Bool?
    var fancy_type: String?
    var owner: Owner?
    var status: Status?
}

    extension AllAuctions {
        func applyUserFilter(_ array: [Auctions]) -> [Auctions] {
            var returnData = array
            guard let filter = CoreDataHandler.fetchFilterSettingsObject() else {
                return returnData
            }
            
            returnData = returnData.filter({ (auction) -> Bool in
                auction.kitty?.is_fancy == filter.is_fancy &&
                    auction.kitty?.is_exclusive == filter.is_fancy &&
                    auction.kitty?.status?.cooldown_index == coolDownSetGlobal[(filter.coolDown)]
            })
            
            returnData = returnData.sorted(by: { (left, right) -> Bool in
                if filter.sortBy == "generation" {
                    return left.kitty!.generation! > right.kitty!.generation!
                } else if filter.sortBy == "cooldown" {
                    return left.kitty!.status!.cooldown! > right.kitty!.status!.cooldown!
                } else if filter.sortBy == "price" {
                    if filter.sortDirection {
                        return left.current_price! > left.current_price!
                    } else {
                        return left.current_price! < left.current_price!
                    }
                } else {
                    return true
                }
            })
            return returnData
        }

}





