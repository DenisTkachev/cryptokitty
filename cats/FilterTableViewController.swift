//
//  FilterTableViewController.swift
//  cats
//
//  Created by Денис on 25.02.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

import UIKit

class FilterTableViewController: UITableViewController {
    var filterSettingsMO: FilterSettingsMO?
    var filterSettingsToSave = FilterSettings()
    
    let sortBy = [NSLocalizedString("GENERATION", comment: ""), NSLocalizedString("COOLDOWN", comment: ""), NSLocalizedString("CURRENTPRICE", comment: "")]
    let sortDirection = [NSLocalizedString("HIGHTTOLOW", comment: ""), NSLocalizedString("LOWTOHIGHT", comment: "")]
    let coolDown = ["FAST","SWIFT","SNAPPY","BRISK","PLODDING","SLOW","SLUGGISH", "CATATONIC"]
    let tableData = [NSLocalizedString("SORTBY", comment: ""),
                     NSLocalizedString("FANCYTYPE", comment: ""),
                     NSLocalizedString("EXCLUSIVE", comment: ""),
                     NSLocalizedString("SORTDIRECTION", comment: ""),
                     NSLocalizedString("COOLDOWN", comment: "")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        
        filterSettingsMO = CoreDataHandler.fetchFilterSettingsObject()
        if let filterSettingsMO = filterSettingsMO {
            filterSettingsToSave = restoreFilter(filterSettingsMO)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !(filterSettingsToSave.coolDown.isEmpty && filterSettingsToSave.sortBy.isEmpty) {
        CoreDataHandler.saveFilterSettings(fillForm: filterSettingsToSave)
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterSettingsMO != nil {
            return tableData.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellSwitch: FilterTableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell") else {
                return UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "FilterTableViewCell") as! FilterTableViewCell
            }
            return cell as! FilterTableViewCell
        }()        
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        cell.selectionStyle = .none
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.0)
        
        guard let wasSavedSettings = filterSettingsMO else {
            return cell
        }
        
        switch indexPath.row {
        case 0: // sortBy
            cell.textLabel?.text = tableData[indexPath.row]
            cell.detailTextLabel?.text = wasSavedSettings.sortBy
            return cell
        case 1:
            cellSwitch.nameLabel.text = tableData[indexPath.row]
            cellSwitch.filterSwitch.addTarget(self, action: #selector(switchChanged(_:)), for: .valueChanged)
            cellSwitch.filterSwitch.tag = indexPath.row
            cellSwitch.filterSwitch.setOn((wasSavedSettings.is_fancy), animated: false)
            return cellSwitch
        case 2:
            cellSwitch.nameLabel.text = tableData[indexPath.row]
            cellSwitch.filterSwitch.addTarget(self, action: #selector(switchChanged(_:)), for: .valueChanged)
            cellSwitch.filterSwitch.tag = indexPath.row
            cellSwitch.filterSwitch.setOn((wasSavedSettings.is_exclusive), animated: false)
            return cellSwitch
        case 3: // SORTDIRECTION
            cell.textLabel?.text = tableData[indexPath.row]
            cell.detailTextLabel?.text = (wasSavedSettings.sortDirection) ? NSLocalizedString("HIGHTTOLOW", comment: "") : NSLocalizedString("LOWTOHIGHT", comment: "")
            return cell
        case 4: // COOLDOWN
            cell.textLabel?.text = tableData[indexPath.row]
            cell.detailTextLabel?.text = wasSavedSettings.coolDown
            return cell
        default: return cell
        }
    }
    
    @objc func switchChanged(_ sender : UISwitch!) {
        if sender.tag == 1 { // fancy type
            filterSettingsToSave.is_fancy = sender.isOn
        } else { // exclusive cell #2
            filterSettingsToSave.is_exclusive = sender.isOn
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 || indexPath.row == 3 || indexPath.row == 4 {
            let picker = PickerViewDialog(textColor: .black, buttonColor: .black, font: .boldSystemFont(ofSize: 12.0), locale: nil, showCancelButton: true)
            picker.show(tableData[indexPath.row], data: pickerData(indexPath.row)) { (selectedValue) in
                let cell = tableView.cellForRow(at: indexPath)
                if selectedValue != nil {
                    cell?.detailTextLabel?.text = selectedValue
                    switch indexPath.row {
                    case 0: self.filterSettingsToSave.sortBy = selectedValue!
                    case 3: self.filterSettingsToSave.sortDirection = ((selectedValue == NSLocalizedString("HIGHTTOLOW", comment: "")) ? true : false)
                    case 4: self.filterSettingsToSave.coolDown = selectedValue!
                    default: break
                    }
                }
            }
        }
    }
    
    func pickerData(_ indexPath: Int) -> [String] {
        let data = [String]()
        if indexPath == 0 {
            return sortBy
        } else if indexPath == 3 {
            return sortDirection
        } else if indexPath == 4 {
            return coolDown
        } else {
            return data
        }
    }
    
    func restoreFilter(_ filter: FilterSettingsMO) -> FilterSettings {
        var savedFilter = FilterSettings()
        savedFilter.coolDown = filter.coolDown
        savedFilter.is_exclusive = filter.is_exclusive
        savedFilter.is_fancy = filter.is_fancy
        savedFilter.sortBy = filter.sortBy
        savedFilter.sortDirection = filter.sortDirection
        return savedFilter
    }
    
}
