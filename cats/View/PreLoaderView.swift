//
//  PreLoaderView2.swift
//  cats
//
//  Created by Денис on 17.02.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

import UIKit

class PreLoaderView: UIView
{
    // MARK: - Properties
    
    private var catFootLayer9: CALayer?
    private var catFootLayer11: CALayer?
    private var catFootLayer13: CALayer?
    private var catFootLayer15: CALayer?
    private var catFootLayer1: CALayer?
    private var catFootLayer5: CALayer?
    private var catFootLayer3: CALayer?
    private var catFootLayer7: CALayer?
    
    private var opacityAnimation: CAKeyframeAnimation?
    private var opacityAnimation1: CAKeyframeAnimation?
    private var opacityAnimation2: CAKeyframeAnimation?
    private var opacityAnimation3: CAKeyframeAnimation?
    private var opacityAnimation4: CAKeyframeAnimation?
    private var opacityAnimation5: CAKeyframeAnimation?
    private var opacityAnimation6: CAKeyframeAnimation?
    private var opacityAnimation7: CAKeyframeAnimation?
    
    // MARK: - Initialization
    
    init()
    {
        super.init(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        self.setupLayers()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.setupLayers()
    }
    
    // MARK: - Setup Layers
    
    private func setupLayers()
    {
        // Colors
        //
        let fillColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        let strokeColor = UIColor.black
        let backgroundColor = UIColor(red: 0.61, green: 0.43615, blue: 0.0244, alpha: 0)
        let borderColor = UIColor(red: 0.81, green: 0.57915, blue: 0.0324, alpha: 0)
        
        // Paths
        //
        let ovalPath = CGMutablePath()
        ovalPath.move(to: CGPoint(x: 3.65, y: 12.552))
        ovalPath.addCurve(to: CGPoint(x: 7.385, y: 7.723), control1: CGPoint(x: 5.713, y: 12.552), control2: CGPoint(x: 7.385, y: 10.39))
        ovalPath.addCurve(to: CGPoint(x: 3.65, y: 2.894), control1: CGPoint(x: 7.385, y: 5.056), control2: CGPoint(x: 5.713, y: 2.894))
        ovalPath.addCurve(to: CGPoint(x: 3.684, y: 1.469), control1: CGPoint(x: 3.543, y: 2.894), control2: CGPoint(x: 3.539, y: 2.134))
        ovalPath.addCurve(to: CGPoint(x: 4.064, y: 0.001), control1: CGPoint(x: 3.83, y: 0.803), control2: CGPoint(x: 4.184, y: -0.028))
        ovalPath.addCurve(to: CGPoint(x: 2.562, y: 1.986), control1: CGPoint(x: 3.976, y: 0.022), control2: CGPoint(x: 3.186, y: 0.834))
        ovalPath.addCurve(to: CGPoint(x: 2.258, y: 3.336), control1: CGPoint(x: 2.283, y: 2.501), control2: CGPoint(x: 2.258, y: 3.336))
        ovalPath.addCurve(to: CGPoint(x: 1.019, y: 4.733), control1: CGPoint(x: 2.258, y: 3.336), control2: CGPoint(x: 1.386, y: 4.107))
        ovalPath.addCurve(to: CGPoint(x: 0, y: 7.614), control1: CGPoint(x: 0.35, y: 5.875), control2: CGPoint(x: 0, y: 6.775))
        ovalPath.addCurve(to: CGPoint(x: 3.65, y: 12.552), control1: CGPoint(x: 0, y: 10.281), control2: CGPoint(x: 1.588, y: 12.552))
        ovalPath.closeSubpath()
        ovalPath.move(to: CGPoint(x: 3.65, y: 12.552))
        
        let ovalPath1 = CGMutablePath()
        ovalPath1.move(to: CGPoint(x: 4.1, y: 14.24))
        ovalPath1.addCurve(to: CGPoint(x: 7.916, y: 8.925), control1: CGPoint(x: 6.032, y: 14.24), control2: CGPoint(x: 7.916, y: 11.992))
        ovalPath1.addCurve(to: CGPoint(x: 4.3, y: 3.325), control1: CGPoint(x: 7.916, y: 5.857), control2: CGPoint(x: 6.233, y: 3.325))
        ovalPath1.addCurve(to: CGPoint(x: 4.29, y: 1.7), control1: CGPoint(x: 4.2, y: 3.325), control2: CGPoint(x: 4.154, y: 2.466))
        ovalPath1.addCurve(to: CGPoint(x: 4.724, y: 0.001), control1: CGPoint(x: 4.426, y: 0.935), control2: CGPoint(x: 4.837, y: -0.032))
        ovalPath1.addCurve(to: CGPoint(x: 3.311, y: 1.58), control1: CGPoint(x: 4.642, y: 0.025), control2: CGPoint(x: 3.895, y: 0.255))
        ovalPath1.addCurve(to: CGPoint(x: 2.834, y: 3.388), control1: CGPoint(x: 3.049, y: 2.173), control2: CGPoint(x: 2.834, y: 3.388))
        ovalPath1.addCurve(to: CGPoint(x: 0.79, y: 5.118), control1: CGPoint(x: 2.834, y: 3.388), control2: CGPoint(x: 1.432, y: 4.147))
        ovalPath1.addCurve(to: CGPoint(x: 0, y: 8.594), control1: CGPoint(x: 0.148, y: 6.089), control2: CGPoint(x: 0, y: 7.628))
        ovalPath1.addCurve(to: CGPoint(x: 4.1, y: 14.24), control1: CGPoint(x: 0, y: 11.662), control2: CGPoint(x: 2.167, y: 14.24))
        ovalPath1.closeSubpath()
        ovalPath1.move(to: CGPoint(x: 4.1, y: 14.24))
        
        let ovalPath2 = CGMutablePath()
        ovalPath2.move(to: CGPoint(x: 3.862, y: 14.733))
        ovalPath2.addCurve(to: CGPoint(x: 8.209, y: 9.411), control1: CGPoint(x: 6.278, y: 14.702), control2: CGPoint(x: 8.209, y: 12.478))
        ovalPath2.addCurve(to: CGPoint(x: 5.674, y: 4.066), control1: CGPoint(x: 8.209, y: 6.343), control2: CGPoint(x: 7.272, y: 4.594))
        ovalPath2.addCurve(to: CGPoint(x: 5.491, y: 2.204), control1: CGPoint(x: 5.576, y: 4.034), control2: CGPoint(x: 5.678, y: 3.162))
        ovalPath2.addCurve(to: CGPoint(x: 3.949, y: 0.001), control1: CGPoint(x: 5.305, y: 1.247), control2: CGPoint(x: 4.062, y: -0.033))
        ovalPath2.addCurve(to: CGPoint(x: 4.233, y: 2.449), control1: CGPoint(x: 3.867, y: 0.025), control2: CGPoint(x: 4.363, y: 0.99))
        ovalPath2.addCurve(to: CGPoint(x: 4.072, y: 3.493), control1: CGPoint(x: 4.2, y: 3.262), control2: CGPoint(x: 4.072, y: 3.493))
        ovalPath2.addCurve(to: CGPoint(x: 1.361, y: 4.904), control1: CGPoint(x: 4.072, y: 3.493), control2: CGPoint(x: 2.116, y: 3.873))
        ovalPath2.addCurve(to: CGPoint(x: 0.003, y: 9.086), control1: CGPoint(x: 0.607, y: 5.935), control2: CGPoint(x: 0.078, y: 6.589))
        ovalPath2.addCurve(to: CGPoint(x: 3.862, y: 14.733), control1: CGPoint(x: -0.073, y: 11.583), control2: CGPoint(x: 1.446, y: 14.765))
        ovalPath2.closeSubpath()
        ovalPath2.move(to: CGPoint(x: 3.862, y: 14.733))
        
        let ovalPath3 = CGMutablePath()
        ovalPath3.move(to: CGPoint(x: 3.171, y: 12.837))
        ovalPath3.addCurve(to: CGPoint(x: 7.413, y: 8.144), control1: CGPoint(x: 5.02, y: 13.007), control2: CGPoint(x: 7.413, y: 11.026))
        ovalPath3.addCurve(to: CGPoint(x: 5.635, y: 3.656), control1: CGPoint(x: 7.413, y: 5.261), control2: CGPoint(x: 6.759, y: 4.397))
        ovalPath3.addCurve(to: CGPoint(x: 5.456, y: 1.749), control1: CGPoint(x: 5.554, y: 3.602), control2: CGPoint(x: 5.759, y: 3.064))
        ovalPath3.addCurve(to: CGPoint(x: 4.304, y: 0.001), control1: CGPoint(x: 5.152, y: 0.433), control2: CGPoint(x: 4.4, y: -0.03))
        ovalPath3.addCurve(to: CGPoint(x: 4.532, y: 1.967), control1: CGPoint(x: 4.234, y: 0.024), control2: CGPoint(x: 4.643, y: 0.596))
        ovalPath3.addCurve(to: CGPoint(x: 4.27, y: 3.089), control1: CGPoint(x: 4.504, y: 2.731), control2: CGPoint(x: 4.27, y: 3.089))
        ovalPath3.addCurve(to: CGPoint(x: 1.543, y: 4.292), control1: CGPoint(x: 4.27, y: 3.089), control2: CGPoint(x: 2.274, y: 3.489))
        ovalPath3.addCurve(to: CGPoint(x: 0.002, y: 8.123), control1: CGPoint(x: 0.813, y: 5.095), control2: CGPoint(x: 0.066, y: 5.777))
        ovalPath3.addCurve(to: CGPoint(x: 3.171, y: 12.837), control1: CGPoint(x: -0.062, y: 10.47), control2: CGPoint(x: 1.322, y: 12.667))
        ovalPath3.closeSubpath()
        ovalPath3.move(to: CGPoint(x: 3.171, y: 12.837))
        
        let ovalPath4 = CGMutablePath()
        ovalPath4.move(to: CGPoint(x: 11.63, y: 16.979))
        ovalPath4.addCurve(to: CGPoint(x: 19.615999, y: 18.271), control1: CGPoint(x: 14.66, y: 16.979), control2: CGPoint(x: 17.348, y: 18.646))
        ovalPath4.addCurve(to: CGPoint(x: 23.587, y: 13.3), control1: CGPoint(x: 21.884001, y: 17.895), control2: CGPoint(x: 23.587, y: 15.573))
        ovalPath4.addCurve(to: CGPoint(x: 22.142, y: 9.384), control1: CGPoint(x: 23.587, y: 12.509), control2: CGPoint(x: 23.216999, y: 10.891))
        ovalPath4.addCurve(to: CGPoint(x: 18.666, y: 5.978), control1: CGPoint(x: 21.066999, y: 7.878), control2: CGPoint(x: 19.483999, y: 7.188))
        ovalPath4.addCurve(to: CGPoint(x: 11.724, y: 0), control1: CGPoint(x: 16.691, y: 3.055), control2: CGPoint(x: 15.141, y: 0))
        ovalPath4.addCurve(to: CGPoint(x: 4.763, y: 5.978), control1: CGPoint(x: 8.891, y: 0), control2: CGPoint(x: 6.899, y: 3.339))
        ovalPath4.addCurve(to: CGPoint(x: 1.307, y: 9.384), control1: CGPoint(x: 3.671, y: 7.327), control2: CGPoint(x: 2.239, y: 8.074))
        ovalPath4.addCurve(to: CGPoint(x: 0, y: 13.643), control1: CGPoint(x: 0.375, y: 10.694), control2: CGPoint(x: 0, y: 12.455))
        ovalPath4.addCurve(to: CGPoint(x: 4.551, y: 18.271), control1: CGPoint(x: 0, y: 16.285999), control2: CGPoint(x: 1.883, y: 18.143999))
        ovalPath4.addCurve(to: CGPoint(x: 11.63, y: 16.979), control1: CGPoint(x: 7.218, y: 18.396999), control2: CGPoint(x: 9.069, y: 16.979))
        ovalPath4.closeSubpath()
        ovalPath4.move(to: CGPoint(x: 11.63, y: 16.979))
        
        let ovalPath5 = CGMutablePath()
        ovalPath5.move(to: CGPoint(x: 3.65, y: 12.552))
        ovalPath5.addCurve(to: CGPoint(x: 7.385, y: 7.723), control1: CGPoint(x: 5.713, y: 12.552), control2: CGPoint(x: 7.385, y: 10.39))
        ovalPath5.addCurve(to: CGPoint(x: 3.65, y: 2.894), control1: CGPoint(x: 7.385, y: 5.056), control2: CGPoint(x: 5.713, y: 2.894))
        ovalPath5.addCurve(to: CGPoint(x: 3.684, y: 1.469), control1: CGPoint(x: 3.543, y: 2.894), control2: CGPoint(x: 3.539, y: 2.134))
        ovalPath5.addCurve(to: CGPoint(x: 4.064, y: 0.001), control1: CGPoint(x: 3.83, y: 0.803), control2: CGPoint(x: 4.184, y: -0.028))
        ovalPath5.addCurve(to: CGPoint(x: 2.562, y: 1.986), control1: CGPoint(x: 3.976, y: 0.022), control2: CGPoint(x: 3.186, y: 0.834))
        ovalPath5.addCurve(to: CGPoint(x: 2.258, y: 3.336), control1: CGPoint(x: 2.283, y: 2.501), control2: CGPoint(x: 2.258, y: 3.336))
        ovalPath5.addCurve(to: CGPoint(x: 1.019, y: 4.733), control1: CGPoint(x: 2.258, y: 3.336), control2: CGPoint(x: 1.386, y: 4.107))
        ovalPath5.addCurve(to: CGPoint(x: 0, y: 7.614), control1: CGPoint(x: 0.35, y: 5.875), control2: CGPoint(x: 0, y: 6.775))
        ovalPath5.addCurve(to: CGPoint(x: 3.65, y: 12.552), control1: CGPoint(x: 0, y: 10.281), control2: CGPoint(x: 1.588, y: 12.552))
        ovalPath5.closeSubpath()
        ovalPath5.move(to: CGPoint(x: 3.65, y: 12.552))
        
        let ovalPath6 = CGMutablePath()
        ovalPath6.move(to: CGPoint(x: 4.1, y: 14.24))
        ovalPath6.addCurve(to: CGPoint(x: 7.916, y: 8.925), control1: CGPoint(x: 6.032, y: 14.24), control2: CGPoint(x: 7.916, y: 11.992))
        ovalPath6.addCurve(to: CGPoint(x: 4.3, y: 3.325), control1: CGPoint(x: 7.916, y: 5.857), control2: CGPoint(x: 6.233, y: 3.325))
        ovalPath6.addCurve(to: CGPoint(x: 4.29, y: 1.7), control1: CGPoint(x: 4.2, y: 3.325), control2: CGPoint(x: 4.154, y: 2.466))
        ovalPath6.addCurve(to: CGPoint(x: 4.724, y: 0.001), control1: CGPoint(x: 4.426, y: 0.935), control2: CGPoint(x: 4.837, y: -0.032))
        ovalPath6.addCurve(to: CGPoint(x: 3.311, y: 1.58), control1: CGPoint(x: 4.642, y: 0.025), control2: CGPoint(x: 3.895, y: 0.255))
        ovalPath6.addCurve(to: CGPoint(x: 2.834, y: 3.388), control1: CGPoint(x: 3.049, y: 2.173), control2: CGPoint(x: 2.834, y: 3.388))
        ovalPath6.addCurve(to: CGPoint(x: 0.79, y: 5.118), control1: CGPoint(x: 2.834, y: 3.388), control2: CGPoint(x: 1.432, y: 4.147))
        ovalPath6.addCurve(to: CGPoint(x: 0, y: 8.594), control1: CGPoint(x: 0.148, y: 6.089), control2: CGPoint(x: 0, y: 7.628))
        ovalPath6.addCurve(to: CGPoint(x: 4.1, y: 14.24), control1: CGPoint(x: 0, y: 11.662), control2: CGPoint(x: 2.167, y: 14.24))
        ovalPath6.closeSubpath()
        ovalPath6.move(to: CGPoint(x: 4.1, y: 14.24))
        
        let ovalPath7 = CGMutablePath()
        ovalPath7.move(to: CGPoint(x: 3.862, y: 14.733))
        ovalPath7.addCurve(to: CGPoint(x: 8.209, y: 9.411), control1: CGPoint(x: 6.278, y: 14.702), control2: CGPoint(x: 8.209, y: 12.478))
        ovalPath7.addCurve(to: CGPoint(x: 5.674, y: 4.066), control1: CGPoint(x: 8.209, y: 6.343), control2: CGPoint(x: 7.272, y: 4.594))
        ovalPath7.addCurve(to: CGPoint(x: 5.491, y: 2.204), control1: CGPoint(x: 5.576, y: 4.034), control2: CGPoint(x: 5.678, y: 3.162))
        ovalPath7.addCurve(to: CGPoint(x: 3.949, y: 0.001), control1: CGPoint(x: 5.305, y: 1.247), control2: CGPoint(x: 4.062, y: -0.033))
        ovalPath7.addCurve(to: CGPoint(x: 4.233, y: 2.449), control1: CGPoint(x: 3.867, y: 0.025), control2: CGPoint(x: 4.363, y: 0.99))
        ovalPath7.addCurve(to: CGPoint(x: 4.072, y: 3.493), control1: CGPoint(x: 4.2, y: 3.262), control2: CGPoint(x: 4.072, y: 3.493))
        ovalPath7.addCurve(to: CGPoint(x: 1.361, y: 4.904), control1: CGPoint(x: 4.072, y: 3.493), control2: CGPoint(x: 2.116, y: 3.873))
        ovalPath7.addCurve(to: CGPoint(x: 0.003, y: 9.086), control1: CGPoint(x: 0.607, y: 5.935), control2: CGPoint(x: 0.078, y: 6.589))
        ovalPath7.addCurve(to: CGPoint(x: 3.862, y: 14.733), control1: CGPoint(x: -0.073, y: 11.583), control2: CGPoint(x: 1.446, y: 14.765))
        ovalPath7.closeSubpath()
        ovalPath7.move(to: CGPoint(x: 3.862, y: 14.733))
        
        let ovalPath8 = CGMutablePath()
        ovalPath8.move(to: CGPoint(x: 3.171, y: 12.837))
        ovalPath8.addCurve(to: CGPoint(x: 7.413, y: 8.144), control1: CGPoint(x: 5.02, y: 13.007), control2: CGPoint(x: 7.413, y: 11.026))
        ovalPath8.addCurve(to: CGPoint(x: 5.635, y: 3.656), control1: CGPoint(x: 7.413, y: 5.261), control2: CGPoint(x: 6.759, y: 4.397))
        ovalPath8.addCurve(to: CGPoint(x: 5.456, y: 1.749), control1: CGPoint(x: 5.554, y: 3.602), control2: CGPoint(x: 5.759, y: 3.064))
        ovalPath8.addCurve(to: CGPoint(x: 4.304, y: 0.001), control1: CGPoint(x: 5.152, y: 0.433), control2: CGPoint(x: 4.4, y: -0.03))
        ovalPath8.addCurve(to: CGPoint(x: 4.532, y: 1.967), control1: CGPoint(x: 4.234, y: 0.024), control2: CGPoint(x: 4.643, y: 0.596))
        ovalPath8.addCurve(to: CGPoint(x: 4.27, y: 3.089), control1: CGPoint(x: 4.504, y: 2.731), control2: CGPoint(x: 4.27, y: 3.089))
        ovalPath8.addCurve(to: CGPoint(x: 1.543, y: 4.292), control1: CGPoint(x: 4.27, y: 3.089), control2: CGPoint(x: 2.274, y: 3.489))
        ovalPath8.addCurve(to: CGPoint(x: 0.002, y: 8.123), control1: CGPoint(x: 0.813, y: 5.095), control2: CGPoint(x: 0.066, y: 5.777))
        ovalPath8.addCurve(to: CGPoint(x: 3.171, y: 12.837), control1: CGPoint(x: -0.062, y: 10.47), control2: CGPoint(x: 1.322, y: 12.667))
        ovalPath8.closeSubpath()
        ovalPath8.move(to: CGPoint(x: 3.171, y: 12.837))
        
        let ovalPath9 = CGMutablePath()
        ovalPath9.move(to: CGPoint(x: 11.63, y: 16.979))
        ovalPath9.addCurve(to: CGPoint(x: 19.615999, y: 18.271), control1: CGPoint(x: 14.66, y: 16.979), control2: CGPoint(x: 17.348, y: 18.646))
        ovalPath9.addCurve(to: CGPoint(x: 23.587, y: 13.3), control1: CGPoint(x: 21.884001, y: 17.895), control2: CGPoint(x: 23.587, y: 15.573))
        ovalPath9.addCurve(to: CGPoint(x: 22.142, y: 9.384), control1: CGPoint(x: 23.587, y: 12.509), control2: CGPoint(x: 23.216999, y: 10.891))
        ovalPath9.addCurve(to: CGPoint(x: 18.666, y: 5.978), control1: CGPoint(x: 21.066999, y: 7.878), control2: CGPoint(x: 19.483999, y: 7.188))
        ovalPath9.addCurve(to: CGPoint(x: 11.724, y: 0), control1: CGPoint(x: 16.691, y: 3.055), control2: CGPoint(x: 15.141, y: 0))
        ovalPath9.addCurve(to: CGPoint(x: 4.763, y: 5.978), control1: CGPoint(x: 8.891, y: 0), control2: CGPoint(x: 6.899, y: 3.339))
        ovalPath9.addCurve(to: CGPoint(x: 1.307, y: 9.384), control1: CGPoint(x: 3.671, y: 7.327), control2: CGPoint(x: 2.239, y: 8.074))
        ovalPath9.addCurve(to: CGPoint(x: 0, y: 13.643), control1: CGPoint(x: 0.375, y: 10.694), control2: CGPoint(x: 0, y: 12.455))
        ovalPath9.addCurve(to: CGPoint(x: 4.551, y: 18.271), control1: CGPoint(x: 0, y: 16.285999), control2: CGPoint(x: 1.883, y: 18.143999))
        ovalPath9.addCurve(to: CGPoint(x: 11.63, y: 16.979), control1: CGPoint(x: 7.218, y: 18.396999), control2: CGPoint(x: 9.069, y: 16.979))
        ovalPath9.closeSubpath()
        ovalPath9.move(to: CGPoint(x: 11.63, y: 16.979))
        
        let ovalPath10 = CGMutablePath()
        ovalPath10.move(to: CGPoint(x: 3.65, y: 12.552))
        ovalPath10.addCurve(to: CGPoint(x: 7.385, y: 7.723), control1: CGPoint(x: 5.713, y: 12.552), control2: CGPoint(x: 7.385, y: 10.39))
        ovalPath10.addCurve(to: CGPoint(x: 3.65, y: 2.894), control1: CGPoint(x: 7.385, y: 5.056), control2: CGPoint(x: 5.713, y: 2.894))
        ovalPath10.addCurve(to: CGPoint(x: 3.684, y: 1.469), control1: CGPoint(x: 3.543, y: 2.894), control2: CGPoint(x: 3.539, y: 2.134))
        ovalPath10.addCurve(to: CGPoint(x: 4.064, y: 0.001), control1: CGPoint(x: 3.83, y: 0.803), control2: CGPoint(x: 4.184, y: -0.028))
        ovalPath10.addCurve(to: CGPoint(x: 2.562, y: 1.986), control1: CGPoint(x: 3.976, y: 0.022), control2: CGPoint(x: 3.186, y: 0.834))
        ovalPath10.addCurve(to: CGPoint(x: 2.258, y: 3.336), control1: CGPoint(x: 2.283, y: 2.501), control2: CGPoint(x: 2.258, y: 3.336))
        ovalPath10.addCurve(to: CGPoint(x: 1.019, y: 4.733), control1: CGPoint(x: 2.258, y: 3.336), control2: CGPoint(x: 1.386, y: 4.107))
        ovalPath10.addCurve(to: CGPoint(x: 0, y: 7.614), control1: CGPoint(x: 0.35, y: 5.875), control2: CGPoint(x: 0, y: 6.775))
        ovalPath10.addCurve(to: CGPoint(x: 3.65, y: 12.552), control1: CGPoint(x: 0, y: 10.281), control2: CGPoint(x: 1.588, y: 12.552))
        ovalPath10.closeSubpath()
        ovalPath10.move(to: CGPoint(x: 3.65, y: 12.552))
        
        let ovalPath11 = CGMutablePath()
        ovalPath11.move(to: CGPoint(x: 4.1, y: 14.24))
        ovalPath11.addCurve(to: CGPoint(x: 7.916, y: 8.925), control1: CGPoint(x: 6.032, y: 14.24), control2: CGPoint(x: 7.916, y: 11.992))
        ovalPath11.addCurve(to: CGPoint(x: 4.3, y: 3.325), control1: CGPoint(x: 7.916, y: 5.857), control2: CGPoint(x: 6.233, y: 3.325))
        ovalPath11.addCurve(to: CGPoint(x: 4.29, y: 1.7), control1: CGPoint(x: 4.2, y: 3.325), control2: CGPoint(x: 4.154, y: 2.466))
        ovalPath11.addCurve(to: CGPoint(x: 4.724, y: 0.001), control1: CGPoint(x: 4.426, y: 0.935), control2: CGPoint(x: 4.837, y: -0.032))
        ovalPath11.addCurve(to: CGPoint(x: 3.311, y: 1.58), control1: CGPoint(x: 4.642, y: 0.025), control2: CGPoint(x: 3.895, y: 0.255))
        ovalPath11.addCurve(to: CGPoint(x: 2.834, y: 3.388), control1: CGPoint(x: 3.049, y: 2.173), control2: CGPoint(x: 2.834, y: 3.388))
        ovalPath11.addCurve(to: CGPoint(x: 0.79, y: 5.118), control1: CGPoint(x: 2.834, y: 3.388), control2: CGPoint(x: 1.432, y: 4.147))
        ovalPath11.addCurve(to: CGPoint(x: 0, y: 8.594), control1: CGPoint(x: 0.148, y: 6.089), control2: CGPoint(x: 0, y: 7.628))
        ovalPath11.addCurve(to: CGPoint(x: 4.1, y: 14.24), control1: CGPoint(x: 0, y: 11.662), control2: CGPoint(x: 2.167, y: 14.24))
        ovalPath11.closeSubpath()
        ovalPath11.move(to: CGPoint(x: 4.1, y: 14.24))
        
        let ovalPath12 = CGMutablePath()
        ovalPath12.move(to: CGPoint(x: 3.862, y: 14.733))
        ovalPath12.addCurve(to: CGPoint(x: 8.209, y: 9.411), control1: CGPoint(x: 6.278, y: 14.702), control2: CGPoint(x: 8.209, y: 12.478))
        ovalPath12.addCurve(to: CGPoint(x: 5.674, y: 4.066), control1: CGPoint(x: 8.209, y: 6.343), control2: CGPoint(x: 7.272, y: 4.594))
        ovalPath12.addCurve(to: CGPoint(x: 5.491, y: 2.204), control1: CGPoint(x: 5.576, y: 4.034), control2: CGPoint(x: 5.678, y: 3.162))
        ovalPath12.addCurve(to: CGPoint(x: 3.949, y: 0.001), control1: CGPoint(x: 5.305, y: 1.247), control2: CGPoint(x: 4.062, y: -0.033))
        ovalPath12.addCurve(to: CGPoint(x: 4.233, y: 2.449), control1: CGPoint(x: 3.867, y: 0.025), control2: CGPoint(x: 4.363, y: 0.99))
        ovalPath12.addCurve(to: CGPoint(x: 4.072, y: 3.493), control1: CGPoint(x: 4.2, y: 3.262), control2: CGPoint(x: 4.072, y: 3.493))
        ovalPath12.addCurve(to: CGPoint(x: 1.361, y: 4.904), control1: CGPoint(x: 4.072, y: 3.493), control2: CGPoint(x: 2.116, y: 3.873))
        ovalPath12.addCurve(to: CGPoint(x: 0.003, y: 9.086), control1: CGPoint(x: 0.607, y: 5.935), control2: CGPoint(x: 0.078, y: 6.589))
        ovalPath12.addCurve(to: CGPoint(x: 3.862, y: 14.733), control1: CGPoint(x: -0.073, y: 11.583), control2: CGPoint(x: 1.446, y: 14.765))
        ovalPath12.closeSubpath()
        ovalPath12.move(to: CGPoint(x: 3.862, y: 14.733))
        
        let ovalPath13 = CGMutablePath()
        ovalPath13.move(to: CGPoint(x: 3.171, y: 12.837))
        ovalPath13.addCurve(to: CGPoint(x: 7.413, y: 8.144), control1: CGPoint(x: 5.02, y: 13.007), control2: CGPoint(x: 7.413, y: 11.026))
        ovalPath13.addCurve(to: CGPoint(x: 5.635, y: 3.656), control1: CGPoint(x: 7.413, y: 5.261), control2: CGPoint(x: 6.759, y: 4.397))
        ovalPath13.addCurve(to: CGPoint(x: 5.456, y: 1.749), control1: CGPoint(x: 5.554, y: 3.602), control2: CGPoint(x: 5.759, y: 3.064))
        ovalPath13.addCurve(to: CGPoint(x: 4.304, y: 0.001), control1: CGPoint(x: 5.152, y: 0.433), control2: CGPoint(x: 4.4, y: -0.03))
        ovalPath13.addCurve(to: CGPoint(x: 4.532, y: 1.967), control1: CGPoint(x: 4.234, y: 0.024), control2: CGPoint(x: 4.643, y: 0.596))
        ovalPath13.addCurve(to: CGPoint(x: 4.27, y: 3.089), control1: CGPoint(x: 4.504, y: 2.731), control2: CGPoint(x: 4.27, y: 3.089))
        ovalPath13.addCurve(to: CGPoint(x: 1.543, y: 4.292), control1: CGPoint(x: 4.27, y: 3.089), control2: CGPoint(x: 2.274, y: 3.489))
        ovalPath13.addCurve(to: CGPoint(x: 0.002, y: 8.123), control1: CGPoint(x: 0.813, y: 5.095), control2: CGPoint(x: 0.066, y: 5.777))
        ovalPath13.addCurve(to: CGPoint(x: 3.171, y: 12.837), control1: CGPoint(x: -0.062, y: 10.47), control2: CGPoint(x: 1.322, y: 12.667))
        ovalPath13.closeSubpath()
        ovalPath13.move(to: CGPoint(x: 3.171, y: 12.837))
        
        let ovalPath14 = CGMutablePath()
        ovalPath14.move(to: CGPoint(x: 11.63, y: 16.979))
        ovalPath14.addCurve(to: CGPoint(x: 19.615999, y: 18.271), control1: CGPoint(x: 14.66, y: 16.979), control2: CGPoint(x: 17.348, y: 18.646))
        ovalPath14.addCurve(to: CGPoint(x: 23.587, y: 13.3), control1: CGPoint(x: 21.884001, y: 17.895), control2: CGPoint(x: 23.587, y: 15.573))
        ovalPath14.addCurve(to: CGPoint(x: 22.142, y: 9.384), control1: CGPoint(x: 23.587, y: 12.509), control2: CGPoint(x: 23.216999, y: 10.891))
        ovalPath14.addCurve(to: CGPoint(x: 18.666, y: 5.978), control1: CGPoint(x: 21.066999, y: 7.878), control2: CGPoint(x: 19.483999, y: 7.188))
        ovalPath14.addCurve(to: CGPoint(x: 11.724, y: 0), control1: CGPoint(x: 16.691, y: 3.055), control2: CGPoint(x: 15.141, y: 0))
        ovalPath14.addCurve(to: CGPoint(x: 4.763, y: 5.978), control1: CGPoint(x: 8.891, y: 0), control2: CGPoint(x: 6.899, y: 3.339))
        ovalPath14.addCurve(to: CGPoint(x: 1.307, y: 9.384), control1: CGPoint(x: 3.671, y: 7.327), control2: CGPoint(x: 2.239, y: 8.074))
        ovalPath14.addCurve(to: CGPoint(x: 0, y: 13.643), control1: CGPoint(x: 0.375, y: 10.694), control2: CGPoint(x: 0, y: 12.455))
        ovalPath14.addCurve(to: CGPoint(x: 4.551, y: 18.271), control1: CGPoint(x: 0, y: 16.285999), control2: CGPoint(x: 1.883, y: 18.143999))
        ovalPath14.addCurve(to: CGPoint(x: 11.63, y: 16.979), control1: CGPoint(x: 7.218, y: 18.396999), control2: CGPoint(x: 9.069, y: 16.979))
        ovalPath14.closeSubpath()
        ovalPath14.move(to: CGPoint(x: 11.63, y: 16.979))
        
        let ovalPath15 = CGMutablePath()
        ovalPath15.move(to: CGPoint(x: 3.65, y: 12.552))
        ovalPath15.addCurve(to: CGPoint(x: 7.385, y: 7.723), control1: CGPoint(x: 5.713, y: 12.552), control2: CGPoint(x: 7.385, y: 10.39))
        ovalPath15.addCurve(to: CGPoint(x: 3.65, y: 2.894), control1: CGPoint(x: 7.385, y: 5.056), control2: CGPoint(x: 5.713, y: 2.894))
        ovalPath15.addCurve(to: CGPoint(x: 3.684, y: 1.469), control1: CGPoint(x: 3.543, y: 2.894), control2: CGPoint(x: 3.539, y: 2.134))
        ovalPath15.addCurve(to: CGPoint(x: 4.064, y: 0.001), control1: CGPoint(x: 3.83, y: 0.803), control2: CGPoint(x: 4.184, y: -0.028))
        ovalPath15.addCurve(to: CGPoint(x: 2.562, y: 1.986), control1: CGPoint(x: 3.976, y: 0.022), control2: CGPoint(x: 3.186, y: 0.834))
        ovalPath15.addCurve(to: CGPoint(x: 2.258, y: 3.336), control1: CGPoint(x: 2.283, y: 2.501), control2: CGPoint(x: 2.258, y: 3.336))
        ovalPath15.addCurve(to: CGPoint(x: 1.019, y: 4.733), control1: CGPoint(x: 2.258, y: 3.336), control2: CGPoint(x: 1.386, y: 4.107))
        ovalPath15.addCurve(to: CGPoint(x: 0, y: 7.614), control1: CGPoint(x: 0.35, y: 5.875), control2: CGPoint(x: 0, y: 6.775))
        ovalPath15.addCurve(to: CGPoint(x: 3.65, y: 12.552), control1: CGPoint(x: 0, y: 10.281), control2: CGPoint(x: 1.588, y: 12.552))
        ovalPath15.closeSubpath()
        ovalPath15.move(to: CGPoint(x: 3.65, y: 12.552))
        
        let ovalPath16 = CGMutablePath()
        ovalPath16.move(to: CGPoint(x: 4.1, y: 14.24))
        ovalPath16.addCurve(to: CGPoint(x: 7.916, y: 8.925), control1: CGPoint(x: 6.032, y: 14.24), control2: CGPoint(x: 7.916, y: 11.992))
        ovalPath16.addCurve(to: CGPoint(x: 4.3, y: 3.325), control1: CGPoint(x: 7.916, y: 5.857), control2: CGPoint(x: 6.233, y: 3.325))
        ovalPath16.addCurve(to: CGPoint(x: 4.29, y: 1.7), control1: CGPoint(x: 4.2, y: 3.325), control2: CGPoint(x: 4.154, y: 2.466))
        ovalPath16.addCurve(to: CGPoint(x: 4.724, y: 0.001), control1: CGPoint(x: 4.426, y: 0.935), control2: CGPoint(x: 4.837, y: -0.032))
        ovalPath16.addCurve(to: CGPoint(x: 3.311, y: 1.58), control1: CGPoint(x: 4.642, y: 0.025), control2: CGPoint(x: 3.895, y: 0.255))
        ovalPath16.addCurve(to: CGPoint(x: 2.834, y: 3.388), control1: CGPoint(x: 3.049, y: 2.173), control2: CGPoint(x: 2.834, y: 3.388))
        ovalPath16.addCurve(to: CGPoint(x: 0.79, y: 5.118), control1: CGPoint(x: 2.834, y: 3.388), control2: CGPoint(x: 1.432, y: 4.147))
        ovalPath16.addCurve(to: CGPoint(x: 0, y: 8.594), control1: CGPoint(x: 0.148, y: 6.089), control2: CGPoint(x: 0, y: 7.628))
        ovalPath16.addCurve(to: CGPoint(x: 4.1, y: 14.24), control1: CGPoint(x: 0, y: 11.662), control2: CGPoint(x: 2.167, y: 14.24))
        ovalPath16.closeSubpath()
        ovalPath16.move(to: CGPoint(x: 4.1, y: 14.24))
        
        let ovalPath17 = CGMutablePath()
        ovalPath17.move(to: CGPoint(x: 3.862, y: 14.733))
        ovalPath17.addCurve(to: CGPoint(x: 8.209, y: 9.411), control1: CGPoint(x: 6.278, y: 14.702), control2: CGPoint(x: 8.209, y: 12.478))
        ovalPath17.addCurve(to: CGPoint(x: 5.674, y: 4.066), control1: CGPoint(x: 8.209, y: 6.343), control2: CGPoint(x: 7.272, y: 4.594))
        ovalPath17.addCurve(to: CGPoint(x: 5.491, y: 2.204), control1: CGPoint(x: 5.576, y: 4.034), control2: CGPoint(x: 5.678, y: 3.162))
        ovalPath17.addCurve(to: CGPoint(x: 3.949, y: 0.001), control1: CGPoint(x: 5.305, y: 1.247), control2: CGPoint(x: 4.062, y: -0.033))
        ovalPath17.addCurve(to: CGPoint(x: 4.233, y: 2.449), control1: CGPoint(x: 3.867, y: 0.025), control2: CGPoint(x: 4.363, y: 0.99))
        ovalPath17.addCurve(to: CGPoint(x: 4.072, y: 3.493), control1: CGPoint(x: 4.2, y: 3.262), control2: CGPoint(x: 4.072, y: 3.493))
        ovalPath17.addCurve(to: CGPoint(x: 1.361, y: 4.904), control1: CGPoint(x: 4.072, y: 3.493), control2: CGPoint(x: 2.116, y: 3.873))
        ovalPath17.addCurve(to: CGPoint(x: 0.003, y: 9.086), control1: CGPoint(x: 0.607, y: 5.935), control2: CGPoint(x: 0.078, y: 6.589))
        ovalPath17.addCurve(to: CGPoint(x: 3.862, y: 14.733), control1: CGPoint(x: -0.073, y: 11.583), control2: CGPoint(x: 1.446, y: 14.765))
        ovalPath17.closeSubpath()
        ovalPath17.move(to: CGPoint(x: 3.862, y: 14.733))
        
        let ovalPath18 = CGMutablePath()
        ovalPath18.move(to: CGPoint(x: 3.171, y: 12.837))
        ovalPath18.addCurve(to: CGPoint(x: 7.413, y: 8.144), control1: CGPoint(x: 5.02, y: 13.007), control2: CGPoint(x: 7.413, y: 11.026))
        ovalPath18.addCurve(to: CGPoint(x: 5.635, y: 3.656), control1: CGPoint(x: 7.413, y: 5.261), control2: CGPoint(x: 6.759, y: 4.397))
        ovalPath18.addCurve(to: CGPoint(x: 5.456, y: 1.749), control1: CGPoint(x: 5.554, y: 3.602), control2: CGPoint(x: 5.759, y: 3.064))
        ovalPath18.addCurve(to: CGPoint(x: 4.304, y: 0.001), control1: CGPoint(x: 5.152, y: 0.433), control2: CGPoint(x: 4.4, y: -0.03))
        ovalPath18.addCurve(to: CGPoint(x: 4.532, y: 1.967), control1: CGPoint(x: 4.234, y: 0.024), control2: CGPoint(x: 4.643, y: 0.596))
        ovalPath18.addCurve(to: CGPoint(x: 4.27, y: 3.089), control1: CGPoint(x: 4.504, y: 2.731), control2: CGPoint(x: 4.27, y: 3.089))
        ovalPath18.addCurve(to: CGPoint(x: 1.543, y: 4.292), control1: CGPoint(x: 4.27, y: 3.089), control2: CGPoint(x: 2.274, y: 3.489))
        ovalPath18.addCurve(to: CGPoint(x: 0.002, y: 8.123), control1: CGPoint(x: 0.813, y: 5.095), control2: CGPoint(x: 0.066, y: 5.777))
        ovalPath18.addCurve(to: CGPoint(x: 3.171, y: 12.837), control1: CGPoint(x: -0.062, y: 10.47), control2: CGPoint(x: 1.322, y: 12.667))
        ovalPath18.closeSubpath()
        ovalPath18.move(to: CGPoint(x: 3.171, y: 12.837))
        
        let ovalPath19 = CGMutablePath()
        ovalPath19.move(to: CGPoint(x: 11.63, y: 16.979))
        ovalPath19.addCurve(to: CGPoint(x: 19.615999, y: 18.271), control1: CGPoint(x: 14.66, y: 16.979), control2: CGPoint(x: 17.348, y: 18.646))
        ovalPath19.addCurve(to: CGPoint(x: 23.587, y: 13.3), control1: CGPoint(x: 21.884001, y: 17.895), control2: CGPoint(x: 23.587, y: 15.573))
        ovalPath19.addCurve(to: CGPoint(x: 22.142, y: 9.384), control1: CGPoint(x: 23.587, y: 12.509), control2: CGPoint(x: 23.216999, y: 10.891))
        ovalPath19.addCurve(to: CGPoint(x: 18.666, y: 5.978), control1: CGPoint(x: 21.066999, y: 7.878), control2: CGPoint(x: 19.483999, y: 7.188))
        ovalPath19.addCurve(to: CGPoint(x: 11.724, y: 0), control1: CGPoint(x: 16.691, y: 3.055), control2: CGPoint(x: 15.141, y: 0))
        ovalPath19.addCurve(to: CGPoint(x: 4.763, y: 5.978), control1: CGPoint(x: 8.891, y: 0), control2: CGPoint(x: 6.899, y: 3.339))
        ovalPath19.addCurve(to: CGPoint(x: 1.307, y: 9.384), control1: CGPoint(x: 3.671, y: 7.327), control2: CGPoint(x: 2.239, y: 8.074))
        ovalPath19.addCurve(to: CGPoint(x: 0, y: 13.643), control1: CGPoint(x: 0.375, y: 10.694), control2: CGPoint(x: 0, y: 12.455))
        ovalPath19.addCurve(to: CGPoint(x: 4.551, y: 18.271), control1: CGPoint(x: 0, y: 16.285999), control2: CGPoint(x: 1.883, y: 18.143999))
        ovalPath19.addCurve(to: CGPoint(x: 11.63, y: 16.979), control1: CGPoint(x: 7.218, y: 18.396999), control2: CGPoint(x: 9.069, y: 16.979))
        ovalPath19.closeSubpath()
        ovalPath19.move(to: CGPoint(x: 11.63, y: 16.979))
        
        let ovalPath20 = CGMutablePath()
        ovalPath20.move(to: CGPoint(x: 3.65, y: 12.552))
        ovalPath20.addCurve(to: CGPoint(x: 7.385, y: 7.723), control1: CGPoint(x: 5.713, y: 12.552), control2: CGPoint(x: 7.385, y: 10.39))
        ovalPath20.addCurve(to: CGPoint(x: 3.65, y: 2.894), control1: CGPoint(x: 7.385, y: 5.056), control2: CGPoint(x: 5.713, y: 2.894))
        ovalPath20.addCurve(to: CGPoint(x: 3.684, y: 1.469), control1: CGPoint(x: 3.543, y: 2.894), control2: CGPoint(x: 3.539, y: 2.134))
        ovalPath20.addCurve(to: CGPoint(x: 4.064, y: 0.001), control1: CGPoint(x: 3.83, y: 0.803), control2: CGPoint(x: 4.184, y: -0.028))
        ovalPath20.addCurve(to: CGPoint(x: 2.562, y: 1.986), control1: CGPoint(x: 3.976, y: 0.022), control2: CGPoint(x: 3.186, y: 0.834))
        ovalPath20.addCurve(to: CGPoint(x: 2.258, y: 3.336), control1: CGPoint(x: 2.283, y: 2.501), control2: CGPoint(x: 2.258, y: 3.336))
        ovalPath20.addCurve(to: CGPoint(x: 1.019, y: 4.733), control1: CGPoint(x: 2.258, y: 3.336), control2: CGPoint(x: 1.386, y: 4.107))
        ovalPath20.addCurve(to: CGPoint(x: 0, y: 7.614), control1: CGPoint(x: 0.35, y: 5.875), control2: CGPoint(x: 0, y: 6.775))
        ovalPath20.addCurve(to: CGPoint(x: 3.65, y: 12.552), control1: CGPoint(x: 0, y: 10.281), control2: CGPoint(x: 1.588, y: 12.552))
        ovalPath20.closeSubpath()
        ovalPath20.move(to: CGPoint(x: 3.65, y: 12.552))
        
        let ovalPath21 = CGMutablePath()
        ovalPath21.move(to: CGPoint(x: 4.1, y: 14.24))
        ovalPath21.addCurve(to: CGPoint(x: 7.916, y: 8.925), control1: CGPoint(x: 6.032, y: 14.24), control2: CGPoint(x: 7.916, y: 11.992))
        ovalPath21.addCurve(to: CGPoint(x: 4.3, y: 3.325), control1: CGPoint(x: 7.916, y: 5.857), control2: CGPoint(x: 6.233, y: 3.325))
        ovalPath21.addCurve(to: CGPoint(x: 4.29, y: 1.7), control1: CGPoint(x: 4.2, y: 3.325), control2: CGPoint(x: 4.154, y: 2.466))
        ovalPath21.addCurve(to: CGPoint(x: 4.724, y: 0.001), control1: CGPoint(x: 4.426, y: 0.935), control2: CGPoint(x: 4.837, y: -0.032))
        ovalPath21.addCurve(to: CGPoint(x: 3.311, y: 1.58), control1: CGPoint(x: 4.642, y: 0.025), control2: CGPoint(x: 3.895, y: 0.255))
        ovalPath21.addCurve(to: CGPoint(x: 2.834, y: 3.388), control1: CGPoint(x: 3.049, y: 2.173), control2: CGPoint(x: 2.834, y: 3.388))
        ovalPath21.addCurve(to: CGPoint(x: 0.79, y: 5.118), control1: CGPoint(x: 2.834, y: 3.388), control2: CGPoint(x: 1.432, y: 4.147))
        ovalPath21.addCurve(to: CGPoint(x: 0, y: 8.594), control1: CGPoint(x: 0.148, y: 6.089), control2: CGPoint(x: 0, y: 7.628))
        ovalPath21.addCurve(to: CGPoint(x: 4.1, y: 14.24), control1: CGPoint(x: 0, y: 11.662), control2: CGPoint(x: 2.167, y: 14.24))
        ovalPath21.closeSubpath()
        ovalPath21.move(to: CGPoint(x: 4.1, y: 14.24))
        
        let ovalPath22 = CGMutablePath()
        ovalPath22.move(to: CGPoint(x: 3.862, y: 14.733))
        ovalPath22.addCurve(to: CGPoint(x: 8.209, y: 9.411), control1: CGPoint(x: 6.278, y: 14.702), control2: CGPoint(x: 8.209, y: 12.478))
        ovalPath22.addCurve(to: CGPoint(x: 5.674, y: 4.066), control1: CGPoint(x: 8.209, y: 6.343), control2: CGPoint(x: 7.272, y: 4.594))
        ovalPath22.addCurve(to: CGPoint(x: 5.491, y: 2.204), control1: CGPoint(x: 5.576, y: 4.034), control2: CGPoint(x: 5.678, y: 3.162))
        ovalPath22.addCurve(to: CGPoint(x: 3.949, y: 0.001), control1: CGPoint(x: 5.305, y: 1.247), control2: CGPoint(x: 4.062, y: -0.033))
        ovalPath22.addCurve(to: CGPoint(x: 4.233, y: 2.449), control1: CGPoint(x: 3.867, y: 0.025), control2: CGPoint(x: 4.363, y: 0.99))
        ovalPath22.addCurve(to: CGPoint(x: 4.072, y: 3.493), control1: CGPoint(x: 4.2, y: 3.262), control2: CGPoint(x: 4.072, y: 3.493))
        ovalPath22.addCurve(to: CGPoint(x: 1.361, y: 4.904), control1: CGPoint(x: 4.072, y: 3.493), control2: CGPoint(x: 2.116, y: 3.873))
        ovalPath22.addCurve(to: CGPoint(x: 0.003, y: 9.086), control1: CGPoint(x: 0.607, y: 5.935), control2: CGPoint(x: 0.078, y: 6.589))
        ovalPath22.addCurve(to: CGPoint(x: 3.862, y: 14.733), control1: CGPoint(x: -0.073, y: 11.583), control2: CGPoint(x: 1.446, y: 14.765))
        ovalPath22.closeSubpath()
        ovalPath22.move(to: CGPoint(x: 3.862, y: 14.733))
        
        let ovalPath23 = CGMutablePath()
        ovalPath23.move(to: CGPoint(x: 3.171, y: 12.837))
        ovalPath23.addCurve(to: CGPoint(x: 7.413, y: 8.144), control1: CGPoint(x: 5.02, y: 13.007), control2: CGPoint(x: 7.413, y: 11.026))
        ovalPath23.addCurve(to: CGPoint(x: 5.635, y: 3.656), control1: CGPoint(x: 7.413, y: 5.261), control2: CGPoint(x: 6.759, y: 4.397))
        ovalPath23.addCurve(to: CGPoint(x: 5.456, y: 1.749), control1: CGPoint(x: 5.554, y: 3.602), control2: CGPoint(x: 5.759, y: 3.064))
        ovalPath23.addCurve(to: CGPoint(x: 4.304, y: 0.001), control1: CGPoint(x: 5.152, y: 0.433), control2: CGPoint(x: 4.4, y: -0.03))
        ovalPath23.addCurve(to: CGPoint(x: 4.532, y: 1.967), control1: CGPoint(x: 4.234, y: 0.024), control2: CGPoint(x: 4.643, y: 0.596))
        ovalPath23.addCurve(to: CGPoint(x: 4.27, y: 3.089), control1: CGPoint(x: 4.504, y: 2.731), control2: CGPoint(x: 4.27, y: 3.089))
        ovalPath23.addCurve(to: CGPoint(x: 1.543, y: 4.292), control1: CGPoint(x: 4.27, y: 3.089), control2: CGPoint(x: 2.274, y: 3.489))
        ovalPath23.addCurve(to: CGPoint(x: 0.002, y: 8.123), control1: CGPoint(x: 0.813, y: 5.095), control2: CGPoint(x: 0.066, y: 5.777))
        ovalPath23.addCurve(to: CGPoint(x: 3.171, y: 12.837), control1: CGPoint(x: -0.062, y: 10.47), control2: CGPoint(x: 1.322, y: 12.667))
        ovalPath23.closeSubpath()
        ovalPath23.move(to: CGPoint(x: 3.171, y: 12.837))
        
        let ovalPath24 = CGMutablePath()
        ovalPath24.move(to: CGPoint(x: 11.63, y: 16.979))
        ovalPath24.addCurve(to: CGPoint(x: 19.615999, y: 18.271), control1: CGPoint(x: 14.66, y: 16.979), control2: CGPoint(x: 17.348, y: 18.646))
        ovalPath24.addCurve(to: CGPoint(x: 23.587, y: 13.3), control1: CGPoint(x: 21.884001, y: 17.895), control2: CGPoint(x: 23.587, y: 15.573))
        ovalPath24.addCurve(to: CGPoint(x: 22.142, y: 9.384), control1: CGPoint(x: 23.587, y: 12.509), control2: CGPoint(x: 23.216999, y: 10.891))
        ovalPath24.addCurve(to: CGPoint(x: 18.666, y: 5.978), control1: CGPoint(x: 21.066999, y: 7.878), control2: CGPoint(x: 19.483999, y: 7.188))
        ovalPath24.addCurve(to: CGPoint(x: 11.724, y: 0), control1: CGPoint(x: 16.691, y: 3.055), control2: CGPoint(x: 15.141, y: 0))
        ovalPath24.addCurve(to: CGPoint(x: 4.763, y: 5.978), control1: CGPoint(x: 8.891, y: 0), control2: CGPoint(x: 6.899, y: 3.339))
        ovalPath24.addCurve(to: CGPoint(x: 1.307, y: 9.384), control1: CGPoint(x: 3.671, y: 7.327), control2: CGPoint(x: 2.239, y: 8.074))
        ovalPath24.addCurve(to: CGPoint(x: 0, y: 13.643), control1: CGPoint(x: 0.375, y: 10.694), control2: CGPoint(x: 0, y: 12.455))
        ovalPath24.addCurve(to: CGPoint(x: 4.551, y: 18.271), control1: CGPoint(x: 0, y: 16.285999), control2: CGPoint(x: 1.883, y: 18.143999))
        ovalPath24.addCurve(to: CGPoint(x: 11.63, y: 16.979), control1: CGPoint(x: 7.218, y: 18.396999), control2: CGPoint(x: 9.069, y: 16.979))
        ovalPath24.closeSubpath()
        ovalPath24.move(to: CGPoint(x: 11.63, y: 16.979))
        
        let ovalPath25 = CGMutablePath()
        ovalPath25.move(to: CGPoint(x: 3.65, y: 12.552))
        ovalPath25.addCurve(to: CGPoint(x: 7.385, y: 7.723), control1: CGPoint(x: 5.713, y: 12.552), control2: CGPoint(x: 7.385, y: 10.39))
        ovalPath25.addCurve(to: CGPoint(x: 3.65, y: 2.894), control1: CGPoint(x: 7.385, y: 5.056), control2: CGPoint(x: 5.713, y: 2.894))
        ovalPath25.addCurve(to: CGPoint(x: 3.684, y: 1.469), control1: CGPoint(x: 3.543, y: 2.894), control2: CGPoint(x: 3.539, y: 2.134))
        ovalPath25.addCurve(to: CGPoint(x: 4.064, y: 0.001), control1: CGPoint(x: 3.83, y: 0.803), control2: CGPoint(x: 4.184, y: -0.028))
        ovalPath25.addCurve(to: CGPoint(x: 2.562, y: 1.986), control1: CGPoint(x: 3.976, y: 0.022), control2: CGPoint(x: 3.186, y: 0.834))
        ovalPath25.addCurve(to: CGPoint(x: 2.258, y: 3.336), control1: CGPoint(x: 2.283, y: 2.501), control2: CGPoint(x: 2.258, y: 3.336))
        ovalPath25.addCurve(to: CGPoint(x: 1.019, y: 4.733), control1: CGPoint(x: 2.258, y: 3.336), control2: CGPoint(x: 1.386, y: 4.107))
        ovalPath25.addCurve(to: CGPoint(x: 0, y: 7.614), control1: CGPoint(x: 0.35, y: 5.875), control2: CGPoint(x: 0, y: 6.775))
        ovalPath25.addCurve(to: CGPoint(x: 3.65, y: 12.552), control1: CGPoint(x: 0, y: 10.281), control2: CGPoint(x: 1.588, y: 12.552))
        ovalPath25.closeSubpath()
        ovalPath25.move(to: CGPoint(x: 3.65, y: 12.552))
        
        let ovalPath26 = CGMutablePath()
        ovalPath26.move(to: CGPoint(x: 4.1, y: 14.24))
        ovalPath26.addCurve(to: CGPoint(x: 7.916, y: 8.925), control1: CGPoint(x: 6.032, y: 14.24), control2: CGPoint(x: 7.916, y: 11.992))
        ovalPath26.addCurve(to: CGPoint(x: 4.3, y: 3.325), control1: CGPoint(x: 7.916, y: 5.857), control2: CGPoint(x: 6.233, y: 3.325))
        ovalPath26.addCurve(to: CGPoint(x: 4.29, y: 1.7), control1: CGPoint(x: 4.2, y: 3.325), control2: CGPoint(x: 4.154, y: 2.466))
        ovalPath26.addCurve(to: CGPoint(x: 4.724, y: 0.001), control1: CGPoint(x: 4.426, y: 0.935), control2: CGPoint(x: 4.837, y: -0.032))
        ovalPath26.addCurve(to: CGPoint(x: 3.311, y: 1.58), control1: CGPoint(x: 4.642, y: 0.025), control2: CGPoint(x: 3.895, y: 0.255))
        ovalPath26.addCurve(to: CGPoint(x: 2.834, y: 3.388), control1: CGPoint(x: 3.049, y: 2.173), control2: CGPoint(x: 2.834, y: 3.388))
        ovalPath26.addCurve(to: CGPoint(x: 0.79, y: 5.118), control1: CGPoint(x: 2.834, y: 3.388), control2: CGPoint(x: 1.432, y: 4.147))
        ovalPath26.addCurve(to: CGPoint(x: 0, y: 8.594), control1: CGPoint(x: 0.148, y: 6.089), control2: CGPoint(x: 0, y: 7.628))
        ovalPath26.addCurve(to: CGPoint(x: 4.1, y: 14.24), control1: CGPoint(x: 0, y: 11.662), control2: CGPoint(x: 2.167, y: 14.24))
        ovalPath26.closeSubpath()
        ovalPath26.move(to: CGPoint(x: 4.1, y: 14.24))
        
        let ovalPath27 = CGMutablePath()
        ovalPath27.move(to: CGPoint(x: 3.862, y: 14.733))
        ovalPath27.addCurve(to: CGPoint(x: 8.209, y: 9.411), control1: CGPoint(x: 6.278, y: 14.702), control2: CGPoint(x: 8.209, y: 12.478))
        ovalPath27.addCurve(to: CGPoint(x: 5.674, y: 4.066), control1: CGPoint(x: 8.209, y: 6.343), control2: CGPoint(x: 7.272, y: 4.594))
        ovalPath27.addCurve(to: CGPoint(x: 5.491, y: 2.204), control1: CGPoint(x: 5.576, y: 4.034), control2: CGPoint(x: 5.678, y: 3.162))
        ovalPath27.addCurve(to: CGPoint(x: 3.949, y: 0.001), control1: CGPoint(x: 5.305, y: 1.247), control2: CGPoint(x: 4.062, y: -0.033))
        ovalPath27.addCurve(to: CGPoint(x: 4.233, y: 2.449), control1: CGPoint(x: 3.867, y: 0.025), control2: CGPoint(x: 4.363, y: 0.99))
        ovalPath27.addCurve(to: CGPoint(x: 4.072, y: 3.493), control1: CGPoint(x: 4.2, y: 3.262), control2: CGPoint(x: 4.072, y: 3.493))
        ovalPath27.addCurve(to: CGPoint(x: 1.361, y: 4.904), control1: CGPoint(x: 4.072, y: 3.493), control2: CGPoint(x: 2.116, y: 3.873))
        ovalPath27.addCurve(to: CGPoint(x: 0.003, y: 9.086), control1: CGPoint(x: 0.607, y: 5.935), control2: CGPoint(x: 0.078, y: 6.589))
        ovalPath27.addCurve(to: CGPoint(x: 3.862, y: 14.733), control1: CGPoint(x: -0.073, y: 11.583), control2: CGPoint(x: 1.446, y: 14.765))
        ovalPath27.closeSubpath()
        ovalPath27.move(to: CGPoint(x: 3.862, y: 14.733))
        
        let ovalPath28 = CGMutablePath()
        ovalPath28.move(to: CGPoint(x: 3.171, y: 12.837))
        ovalPath28.addCurve(to: CGPoint(x: 7.413, y: 8.144), control1: CGPoint(x: 5.02, y: 13.007), control2: CGPoint(x: 7.413, y: 11.026))
        ovalPath28.addCurve(to: CGPoint(x: 5.635, y: 3.656), control1: CGPoint(x: 7.413, y: 5.261), control2: CGPoint(x: 6.759, y: 4.397))
        ovalPath28.addCurve(to: CGPoint(x: 5.456, y: 1.749), control1: CGPoint(x: 5.554, y: 3.602), control2: CGPoint(x: 5.759, y: 3.064))
        ovalPath28.addCurve(to: CGPoint(x: 4.304, y: 0.001), control1: CGPoint(x: 5.152, y: 0.433), control2: CGPoint(x: 4.4, y: -0.03))
        ovalPath28.addCurve(to: CGPoint(x: 4.532, y: 1.967), control1: CGPoint(x: 4.234, y: 0.024), control2: CGPoint(x: 4.643, y: 0.596))
        ovalPath28.addCurve(to: CGPoint(x: 4.27, y: 3.089), control1: CGPoint(x: 4.504, y: 2.731), control2: CGPoint(x: 4.27, y: 3.089))
        ovalPath28.addCurve(to: CGPoint(x: 1.543, y: 4.292), control1: CGPoint(x: 4.27, y: 3.089), control2: CGPoint(x: 2.274, y: 3.489))
        ovalPath28.addCurve(to: CGPoint(x: 0.002, y: 8.123), control1: CGPoint(x: 0.813, y: 5.095), control2: CGPoint(x: 0.066, y: 5.777))
        ovalPath28.addCurve(to: CGPoint(x: 3.171, y: 12.837), control1: CGPoint(x: -0.062, y: 10.47), control2: CGPoint(x: 1.322, y: 12.667))
        ovalPath28.closeSubpath()
        ovalPath28.move(to: CGPoint(x: 3.171, y: 12.837))
        
        let ovalPath29 = CGMutablePath()
        ovalPath29.move(to: CGPoint(x: 11.63, y: 16.979))
        ovalPath29.addCurve(to: CGPoint(x: 19.615999, y: 18.271), control1: CGPoint(x: 14.66, y: 16.979), control2: CGPoint(x: 17.348, y: 18.646))
        ovalPath29.addCurve(to: CGPoint(x: 23.587, y: 13.3), control1: CGPoint(x: 21.884001, y: 17.895), control2: CGPoint(x: 23.587, y: 15.573))
        ovalPath29.addCurve(to: CGPoint(x: 22.142, y: 9.384), control1: CGPoint(x: 23.587, y: 12.509), control2: CGPoint(x: 23.216999, y: 10.891))
        ovalPath29.addCurve(to: CGPoint(x: 18.666, y: 5.978), control1: CGPoint(x: 21.066999, y: 7.878), control2: CGPoint(x: 19.483999, y: 7.188))
        ovalPath29.addCurve(to: CGPoint(x: 11.724, y: 0), control1: CGPoint(x: 16.691, y: 3.055), control2: CGPoint(x: 15.141, y: 0))
        ovalPath29.addCurve(to: CGPoint(x: 4.763, y: 5.978), control1: CGPoint(x: 8.891, y: 0), control2: CGPoint(x: 6.899, y: 3.339))
        ovalPath29.addCurve(to: CGPoint(x: 1.307, y: 9.384), control1: CGPoint(x: 3.671, y: 7.327), control2: CGPoint(x: 2.239, y: 8.074))
        ovalPath29.addCurve(to: CGPoint(x: 0, y: 13.643), control1: CGPoint(x: 0.375, y: 10.694), control2: CGPoint(x: 0, y: 12.455))
        ovalPath29.addCurve(to: CGPoint(x: 4.551, y: 18.271), control1: CGPoint(x: 0, y: 16.285999), control2: CGPoint(x: 1.883, y: 18.143999))
        ovalPath29.addCurve(to: CGPoint(x: 11.63, y: 16.979), control1: CGPoint(x: 7.218, y: 18.396999), control2: CGPoint(x: 9.069, y: 16.979))
        ovalPath29.closeSubpath()
        ovalPath29.move(to: CGPoint(x: 11.63, y: 16.979))
        
        let ovalPath30 = CGMutablePath()
        ovalPath30.move(to: CGPoint(x: 3.65, y: 12.552))
        ovalPath30.addCurve(to: CGPoint(x: 7.385, y: 7.723), control1: CGPoint(x: 5.713, y: 12.552), control2: CGPoint(x: 7.385, y: 10.39))
        ovalPath30.addCurve(to: CGPoint(x: 3.65, y: 2.894), control1: CGPoint(x: 7.385, y: 5.056), control2: CGPoint(x: 5.713, y: 2.894))
        ovalPath30.addCurve(to: CGPoint(x: 3.684, y: 1.469), control1: CGPoint(x: 3.543, y: 2.894), control2: CGPoint(x: 3.539, y: 2.134))
        ovalPath30.addCurve(to: CGPoint(x: 4.064, y: 0.001), control1: CGPoint(x: 3.83, y: 0.803), control2: CGPoint(x: 4.184, y: -0.028))
        ovalPath30.addCurve(to: CGPoint(x: 2.562, y: 1.986), control1: CGPoint(x: 3.976, y: 0.022), control2: CGPoint(x: 3.186, y: 0.834))
        ovalPath30.addCurve(to: CGPoint(x: 2.258, y: 3.336), control1: CGPoint(x: 2.283, y: 2.501), control2: CGPoint(x: 2.258, y: 3.336))
        ovalPath30.addCurve(to: CGPoint(x: 1.019, y: 4.733), control1: CGPoint(x: 2.258, y: 3.336), control2: CGPoint(x: 1.386, y: 4.107))
        ovalPath30.addCurve(to: CGPoint(x: 0, y: 7.614), control1: CGPoint(x: 0.35, y: 5.875), control2: CGPoint(x: 0, y: 6.775))
        ovalPath30.addCurve(to: CGPoint(x: 3.65, y: 12.552), control1: CGPoint(x: 0, y: 10.281), control2: CGPoint(x: 1.588, y: 12.552))
        ovalPath30.closeSubpath()
        ovalPath30.move(to: CGPoint(x: 3.65, y: 12.552))
        
        let ovalPath31 = CGMutablePath()
        ovalPath31.move(to: CGPoint(x: 4.1, y: 14.24))
        ovalPath31.addCurve(to: CGPoint(x: 7.916, y: 8.925), control1: CGPoint(x: 6.032, y: 14.24), control2: CGPoint(x: 7.916, y: 11.992))
        ovalPath31.addCurve(to: CGPoint(x: 4.3, y: 3.325), control1: CGPoint(x: 7.916, y: 5.857), control2: CGPoint(x: 6.233, y: 3.325))
        ovalPath31.addCurve(to: CGPoint(x: 4.29, y: 1.7), control1: CGPoint(x: 4.2, y: 3.325), control2: CGPoint(x: 4.154, y: 2.466))
        ovalPath31.addCurve(to: CGPoint(x: 4.724, y: 0.001), control1: CGPoint(x: 4.426, y: 0.935), control2: CGPoint(x: 4.837, y: -0.032))
        ovalPath31.addCurve(to: CGPoint(x: 3.311, y: 1.58), control1: CGPoint(x: 4.642, y: 0.025), control2: CGPoint(x: 3.895, y: 0.255))
        ovalPath31.addCurve(to: CGPoint(x: 2.834, y: 3.388), control1: CGPoint(x: 3.049, y: 2.173), control2: CGPoint(x: 2.834, y: 3.388))
        ovalPath31.addCurve(to: CGPoint(x: 0.79, y: 5.118), control1: CGPoint(x: 2.834, y: 3.388), control2: CGPoint(x: 1.432, y: 4.147))
        ovalPath31.addCurve(to: CGPoint(x: 0, y: 8.594), control1: CGPoint(x: 0.148, y: 6.089), control2: CGPoint(x: 0, y: 7.628))
        ovalPath31.addCurve(to: CGPoint(x: 4.1, y: 14.24), control1: CGPoint(x: 0, y: 11.662), control2: CGPoint(x: 2.167, y: 14.24))
        ovalPath31.closeSubpath()
        ovalPath31.move(to: CGPoint(x: 4.1, y: 14.24))
        
        let ovalPath32 = CGMutablePath()
        ovalPath32.move(to: CGPoint(x: 3.862, y: 14.733))
        ovalPath32.addCurve(to: CGPoint(x: 8.209, y: 9.411), control1: CGPoint(x: 6.278, y: 14.702), control2: CGPoint(x: 8.209, y: 12.478))
        ovalPath32.addCurve(to: CGPoint(x: 5.674, y: 4.066), control1: CGPoint(x: 8.209, y: 6.343), control2: CGPoint(x: 7.272, y: 4.594))
        ovalPath32.addCurve(to: CGPoint(x: 5.491, y: 2.204), control1: CGPoint(x: 5.576, y: 4.034), control2: CGPoint(x: 5.678, y: 3.162))
        ovalPath32.addCurve(to: CGPoint(x: 3.949, y: 0.001), control1: CGPoint(x: 5.305, y: 1.247), control2: CGPoint(x: 4.062, y: -0.033))
        ovalPath32.addCurve(to: CGPoint(x: 4.233, y: 2.449), control1: CGPoint(x: 3.867, y: 0.025), control2: CGPoint(x: 4.363, y: 0.99))
        ovalPath32.addCurve(to: CGPoint(x: 4.072, y: 3.493), control1: CGPoint(x: 4.2, y: 3.262), control2: CGPoint(x: 4.072, y: 3.493))
        ovalPath32.addCurve(to: CGPoint(x: 1.361, y: 4.904), control1: CGPoint(x: 4.072, y: 3.493), control2: CGPoint(x: 2.116, y: 3.873))
        ovalPath32.addCurve(to: CGPoint(x: 0.003, y: 9.086), control1: CGPoint(x: 0.607, y: 5.935), control2: CGPoint(x: 0.078, y: 6.589))
        ovalPath32.addCurve(to: CGPoint(x: 3.862, y: 14.733), control1: CGPoint(x: -0.073, y: 11.583), control2: CGPoint(x: 1.446, y: 14.765))
        ovalPath32.closeSubpath()
        ovalPath32.move(to: CGPoint(x: 3.862, y: 14.733))
        
        let ovalPath33 = CGMutablePath()
        ovalPath33.move(to: CGPoint(x: 3.171, y: 12.837))
        ovalPath33.addCurve(to: CGPoint(x: 7.413, y: 8.144), control1: CGPoint(x: 5.02, y: 13.007), control2: CGPoint(x: 7.413, y: 11.026))
        ovalPath33.addCurve(to: CGPoint(x: 5.635, y: 3.656), control1: CGPoint(x: 7.413, y: 5.261), control2: CGPoint(x: 6.759, y: 4.397))
        ovalPath33.addCurve(to: CGPoint(x: 5.456, y: 1.749), control1: CGPoint(x: 5.554, y: 3.602), control2: CGPoint(x: 5.759, y: 3.064))
        ovalPath33.addCurve(to: CGPoint(x: 4.304, y: 0.001), control1: CGPoint(x: 5.152, y: 0.433), control2: CGPoint(x: 4.4, y: -0.03))
        ovalPath33.addCurve(to: CGPoint(x: 4.532, y: 1.967), control1: CGPoint(x: 4.234, y: 0.024), control2: CGPoint(x: 4.643, y: 0.596))
        ovalPath33.addCurve(to: CGPoint(x: 4.27, y: 3.089), control1: CGPoint(x: 4.504, y: 2.731), control2: CGPoint(x: 4.27, y: 3.089))
        ovalPath33.addCurve(to: CGPoint(x: 1.543, y: 4.292), control1: CGPoint(x: 4.27, y: 3.089), control2: CGPoint(x: 2.274, y: 3.489))
        ovalPath33.addCurve(to: CGPoint(x: 0.002, y: 8.123), control1: CGPoint(x: 0.813, y: 5.095), control2: CGPoint(x: 0.066, y: 5.777))
        ovalPath33.addCurve(to: CGPoint(x: 3.171, y: 12.837), control1: CGPoint(x: -0.062, y: 10.47), control2: CGPoint(x: 1.322, y: 12.667))
        ovalPath33.closeSubpath()
        ovalPath33.move(to: CGPoint(x: 3.171, y: 12.837))
        
        let ovalPath34 = CGMutablePath()
        ovalPath34.move(to: CGPoint(x: 11.63, y: 16.979))
        ovalPath34.addCurve(to: CGPoint(x: 19.615999, y: 18.271), control1: CGPoint(x: 14.66, y: 16.979), control2: CGPoint(x: 17.348, y: 18.646))
        ovalPath34.addCurve(to: CGPoint(x: 23.587, y: 13.3), control1: CGPoint(x: 21.884001, y: 17.895), control2: CGPoint(x: 23.587, y: 15.573))
        ovalPath34.addCurve(to: CGPoint(x: 22.142, y: 9.384), control1: CGPoint(x: 23.587, y: 12.509), control2: CGPoint(x: 23.216999, y: 10.891))
        ovalPath34.addCurve(to: CGPoint(x: 18.666, y: 5.978), control1: CGPoint(x: 21.066999, y: 7.878), control2: CGPoint(x: 19.483999, y: 7.188))
        ovalPath34.addCurve(to: CGPoint(x: 11.724, y: 0), control1: CGPoint(x: 16.691, y: 3.055), control2: CGPoint(x: 15.141, y: 0))
        ovalPath34.addCurve(to: CGPoint(x: 4.763, y: 5.978), control1: CGPoint(x: 8.891, y: 0), control2: CGPoint(x: 6.899, y: 3.339))
        ovalPath34.addCurve(to: CGPoint(x: 1.307, y: 9.384), control1: CGPoint(x: 3.671, y: 7.327), control2: CGPoint(x: 2.239, y: 8.074))
        ovalPath34.addCurve(to: CGPoint(x: 0, y: 13.643), control1: CGPoint(x: 0.375, y: 10.694), control2: CGPoint(x: 0, y: 12.455))
        ovalPath34.addCurve(to: CGPoint(x: 4.551, y: 18.271), control1: CGPoint(x: 0, y: 16.285999), control2: CGPoint(x: 1.883, y: 18.143999))
        ovalPath34.addCurve(to: CGPoint(x: 11.63, y: 16.979), control1: CGPoint(x: 7.218, y: 18.396999), control2: CGPoint(x: 9.069, y: 16.979))
        ovalPath34.closeSubpath()
        ovalPath34.move(to: CGPoint(x: 11.63, y: 16.979))
        
        let ovalPath35 = CGMutablePath()
        ovalPath35.move(to: CGPoint(x: 3.65, y: 12.552))
        ovalPath35.addCurve(to: CGPoint(x: 7.385, y: 7.723), control1: CGPoint(x: 5.713, y: 12.552), control2: CGPoint(x: 7.385, y: 10.39))
        ovalPath35.addCurve(to: CGPoint(x: 3.65, y: 2.894), control1: CGPoint(x: 7.385, y: 5.056), control2: CGPoint(x: 5.713, y: 2.894))
        ovalPath35.addCurve(to: CGPoint(x: 3.684, y: 1.469), control1: CGPoint(x: 3.543, y: 2.894), control2: CGPoint(x: 3.539, y: 2.134))
        ovalPath35.addCurve(to: CGPoint(x: 4.064, y: 0.001), control1: CGPoint(x: 3.83, y: 0.803), control2: CGPoint(x: 4.184, y: -0.028))
        ovalPath35.addCurve(to: CGPoint(x: 2.562, y: 1.986), control1: CGPoint(x: 3.976, y: 0.022), control2: CGPoint(x: 3.186, y: 0.834))
        ovalPath35.addCurve(to: CGPoint(x: 2.258, y: 3.336), control1: CGPoint(x: 2.283, y: 2.501), control2: CGPoint(x: 2.258, y: 3.336))
        ovalPath35.addCurve(to: CGPoint(x: 1.019, y: 4.733), control1: CGPoint(x: 2.258, y: 3.336), control2: CGPoint(x: 1.386, y: 4.107))
        ovalPath35.addCurve(to: CGPoint(x: 0, y: 7.614), control1: CGPoint(x: 0.35, y: 5.875), control2: CGPoint(x: 0, y: 6.775))
        ovalPath35.addCurve(to: CGPoint(x: 3.65, y: 12.552), control1: CGPoint(x: 0, y: 10.281), control2: CGPoint(x: 1.588, y: 12.552))
        ovalPath35.closeSubpath()
        ovalPath35.move(to: CGPoint(x: 3.65, y: 12.552))
        
        let ovalPath36 = CGMutablePath()
        ovalPath36.move(to: CGPoint(x: 4.1, y: 14.24))
        ovalPath36.addCurve(to: CGPoint(x: 7.916, y: 8.925), control1: CGPoint(x: 6.032, y: 14.24), control2: CGPoint(x: 7.916, y: 11.992))
        ovalPath36.addCurve(to: CGPoint(x: 4.3, y: 3.325), control1: CGPoint(x: 7.916, y: 5.857), control2: CGPoint(x: 6.233, y: 3.325))
        ovalPath36.addCurve(to: CGPoint(x: 4.29, y: 1.7), control1: CGPoint(x: 4.2, y: 3.325), control2: CGPoint(x: 4.154, y: 2.466))
        ovalPath36.addCurve(to: CGPoint(x: 4.724, y: 0.001), control1: CGPoint(x: 4.426, y: 0.935), control2: CGPoint(x: 4.837, y: -0.032))
        ovalPath36.addCurve(to: CGPoint(x: 3.311, y: 1.58), control1: CGPoint(x: 4.642, y: 0.025), control2: CGPoint(x: 3.895, y: 0.255))
        ovalPath36.addCurve(to: CGPoint(x: 2.834, y: 3.388), control1: CGPoint(x: 3.049, y: 2.173), control2: CGPoint(x: 2.834, y: 3.388))
        ovalPath36.addCurve(to: CGPoint(x: 0.79, y: 5.118), control1: CGPoint(x: 2.834, y: 3.388), control2: CGPoint(x: 1.432, y: 4.147))
        ovalPath36.addCurve(to: CGPoint(x: 0, y: 8.594), control1: CGPoint(x: 0.148, y: 6.089), control2: CGPoint(x: 0, y: 7.628))
        ovalPath36.addCurve(to: CGPoint(x: 4.1, y: 14.24), control1: CGPoint(x: 0, y: 11.662), control2: CGPoint(x: 2.167, y: 14.24))
        ovalPath36.closeSubpath()
        ovalPath36.move(to: CGPoint(x: 4.1, y: 14.24))
        
        let ovalPath37 = CGMutablePath()
        ovalPath37.move(to: CGPoint(x: 3.862, y: 14.733))
        ovalPath37.addCurve(to: CGPoint(x: 8.209, y: 9.411), control1: CGPoint(x: 6.278, y: 14.702), control2: CGPoint(x: 8.209, y: 12.478))
        ovalPath37.addCurve(to: CGPoint(x: 5.674, y: 4.066), control1: CGPoint(x: 8.209, y: 6.343), control2: CGPoint(x: 7.272, y: 4.594))
        ovalPath37.addCurve(to: CGPoint(x: 5.491, y: 2.204), control1: CGPoint(x: 5.576, y: 4.034), control2: CGPoint(x: 5.678, y: 3.162))
        ovalPath37.addCurve(to: CGPoint(x: 3.949, y: 0.001), control1: CGPoint(x: 5.305, y: 1.247), control2: CGPoint(x: 4.062, y: -0.033))
        ovalPath37.addCurve(to: CGPoint(x: 4.233, y: 2.449), control1: CGPoint(x: 3.867, y: 0.025), control2: CGPoint(x: 4.363, y: 0.99))
        ovalPath37.addCurve(to: CGPoint(x: 4.072, y: 3.493), control1: CGPoint(x: 4.2, y: 3.262), control2: CGPoint(x: 4.072, y: 3.493))
        ovalPath37.addCurve(to: CGPoint(x: 1.361, y: 4.904), control1: CGPoint(x: 4.072, y: 3.493), control2: CGPoint(x: 2.116, y: 3.873))
        ovalPath37.addCurve(to: CGPoint(x: 0.003, y: 9.086), control1: CGPoint(x: 0.607, y: 5.935), control2: CGPoint(x: 0.078, y: 6.589))
        ovalPath37.addCurve(to: CGPoint(x: 3.862, y: 14.733), control1: CGPoint(x: -0.073, y: 11.583), control2: CGPoint(x: 1.446, y: 14.765))
        ovalPath37.closeSubpath()
        ovalPath37.move(to: CGPoint(x: 3.862, y: 14.733))
        
        let ovalPath38 = CGMutablePath()
        ovalPath38.move(to: CGPoint(x: 3.171, y: 12.837))
        ovalPath38.addCurve(to: CGPoint(x: 7.413, y: 8.144), control1: CGPoint(x: 5.02, y: 13.007), control2: CGPoint(x: 7.413, y: 11.026))
        ovalPath38.addCurve(to: CGPoint(x: 5.635, y: 3.656), control1: CGPoint(x: 7.413, y: 5.261), control2: CGPoint(x: 6.759, y: 4.397))
        ovalPath38.addCurve(to: CGPoint(x: 5.456, y: 1.749), control1: CGPoint(x: 5.554, y: 3.602), control2: CGPoint(x: 5.759, y: 3.064))
        ovalPath38.addCurve(to: CGPoint(x: 4.304, y: 0.001), control1: CGPoint(x: 5.152, y: 0.433), control2: CGPoint(x: 4.4, y: -0.03))
        ovalPath38.addCurve(to: CGPoint(x: 4.532, y: 1.967), control1: CGPoint(x: 4.234, y: 0.024), control2: CGPoint(x: 4.643, y: 0.596))
        ovalPath38.addCurve(to: CGPoint(x: 4.27, y: 3.089), control1: CGPoint(x: 4.504, y: 2.731), control2: CGPoint(x: 4.27, y: 3.089))
        ovalPath38.addCurve(to: CGPoint(x: 1.543, y: 4.292), control1: CGPoint(x: 4.27, y: 3.089), control2: CGPoint(x: 2.274, y: 3.489))
        ovalPath38.addCurve(to: CGPoint(x: 0.002, y: 8.123), control1: CGPoint(x: 0.813, y: 5.095), control2: CGPoint(x: 0.066, y: 5.777))
        ovalPath38.addCurve(to: CGPoint(x: 3.171, y: 12.837), control1: CGPoint(x: -0.062, y: 10.47), control2: CGPoint(x: 1.322, y: 12.667))
        ovalPath38.closeSubpath()
        ovalPath38.move(to: CGPoint(x: 3.171, y: 12.837))
        
        let ovalPath39 = CGMutablePath()
        ovalPath39.move(to: CGPoint(x: 11.63, y: 16.979))
        ovalPath39.addCurve(to: CGPoint(x: 19.615999, y: 18.271), control1: CGPoint(x: 14.66, y: 16.979), control2: CGPoint(x: 17.348, y: 18.646))
        ovalPath39.addCurve(to: CGPoint(x: 23.587, y: 13.3), control1: CGPoint(x: 21.884001, y: 17.895), control2: CGPoint(x: 23.587, y: 15.573))
        ovalPath39.addCurve(to: CGPoint(x: 22.142, y: 9.384), control1: CGPoint(x: 23.587, y: 12.509), control2: CGPoint(x: 23.216999, y: 10.891))
        ovalPath39.addCurve(to: CGPoint(x: 18.666, y: 5.978), control1: CGPoint(x: 21.066999, y: 7.878), control2: CGPoint(x: 19.483999, y: 7.188))
        ovalPath39.addCurve(to: CGPoint(x: 11.724, y: 0), control1: CGPoint(x: 16.691, y: 3.055), control2: CGPoint(x: 15.141, y: 0))
        ovalPath39.addCurve(to: CGPoint(x: 4.763, y: 5.978), control1: CGPoint(x: 8.891, y: 0), control2: CGPoint(x: 6.899, y: 3.339))
        ovalPath39.addCurve(to: CGPoint(x: 1.307, y: 9.384), control1: CGPoint(x: 3.671, y: 7.327), control2: CGPoint(x: 2.239, y: 8.074))
        ovalPath39.addCurve(to: CGPoint(x: 0, y: 13.643), control1: CGPoint(x: 0.375, y: 10.694), control2: CGPoint(x: 0, y: 12.455))
        ovalPath39.addCurve(to: CGPoint(x: 4.551, y: 18.271), control1: CGPoint(x: 0, y: 16.285999), control2: CGPoint(x: 1.883, y: 18.143999))
        ovalPath39.addCurve(to: CGPoint(x: 11.63, y: 16.979), control1: CGPoint(x: 7.218, y: 18.396999), control2: CGPoint(x: 9.069, y: 16.979))
        ovalPath39.closeSubpath()
        ovalPath39.move(to: CGPoint(x: 11.63, y: 16.979))
        
        // CatFoot5
        //
        let catFootLayer = CALayer()
        catFootLayer.name = "CatFoot5"
        catFootLayer.bounds = CGRect(x: 0, y: 0, width: 54, height: 52)
        catFootLayer.position = CGPoint(x: 94, y: 86)
        catFootLayer.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer.contentsGravity = kCAGravityCenter
        catFootLayer.contentsScale = 2
        catFootLayer.transform = CATransform3D( m11: 0.857784, m12: 0.514011, m13: 0, m14: 0,
                                                m21: -0.514011, m22: 0.857784, m23: 0, m24: 0,
                                                m31: 0, m32: 0, m33: 1, m34: 0,
                                                m41: 0, m42: 0, m43: 0, m44: 1 )
        
        // CatFoot5 Sublayers
        //
        
        // CatFoot
        //
        let catFootLayer1 = CALayer()
        catFootLayer1.name = "CatFoot"
        catFootLayer1.bounds = CGRect(x: 0, y: 0, width: 40, height: 35.652174)
        catFootLayer1.position = CGPoint(x: 7, y: 7.754613)
        catFootLayer1.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer1.contentsGravity = kCAGravityCenter
        catFootLayer1.contentsScale = 2
        catFootLayer1.opacity = 0
        
        // CatFoot Animations
        //
        
        // opacity
        //
        let opacityAnimation = CAKeyframeAnimation()
        opacityAnimation.beginTime = 4
        opacityAnimation.duration = 1
        opacityAnimation.fillMode = kCAFillModeForwards
        opacityAnimation.isRemovedOnCompletion = false
        opacityAnimation.keyPath = "opacity"
        opacityAnimation.values = [ 0, 1 ]
        opacityAnimation.calculationMode = kCAAnimationLinear
        
        
        // CatFoot Sublayers
        //
        
        // Oval
        //
        let ovalLayer = CAShapeLayer()
        ovalLayer.name = "Oval"
        ovalLayer.bounds = CGRect(x: 0, y: 0, width: 7.384633, height: 12.551613)
        ovalLayer.position = CGPoint(x: 2.329651, y: 10.05005)
        ovalLayer.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer.contentsGravity = kCAGravityCenter
        ovalLayer.contentsScale = 2
        ovalLayer.path = ovalPath
        ovalLayer.fillColor = fillColor.cgColor
        ovalLayer.strokeColor = strokeColor.cgColor
        ovalLayer.fillRule = kCAFillRuleEvenOdd
        ovalLayer.lineWidth = 1
        
        catFootLayer1.addSublayer(ovalLayer)
        
        // Oval
        //
        let ovalLayer1 = CAShapeLayer()
        ovalLayer1.name = "Oval"
        ovalLayer1.bounds = CGRect(x: 0, y: 0, width: 7.916001, height: 14.240465)
        ovalLayer1.position = CGPoint(x: 10.139501, y: 1.110983)
        ovalLayer1.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer1.contentsGravity = kCAGravityCenter
        ovalLayer1.contentsScale = 2
        ovalLayer1.path = ovalPath1
        ovalLayer1.fillColor = fillColor.cgColor
        ovalLayer1.strokeColor = strokeColor.cgColor
        ovalLayer1.fillRule = kCAFillRuleEvenOdd
        ovalLayer1.lineWidth = 1
        
        catFootLayer1.addSublayer(ovalLayer1)
        
        // Oval
        //
        let ovalLayer2 = CAShapeLayer()
        ovalLayer2.name = "Oval"
        ovalLayer2.bounds = CGRect(x: 0, y: 0, width: 8.208792, height: 14.733717)
        ovalLayer2.position = CGPoint(x: 22.182283, y: 0.712935)
        ovalLayer2.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer2.contentsGravity = kCAGravityCenter
        ovalLayer2.contentsScale = 2
        ovalLayer2.path = ovalPath2
        ovalLayer2.fillColor = fillColor.cgColor
        ovalLayer2.strokeColor = strokeColor.cgColor
        ovalLayer2.fillRule = kCAFillRuleEvenOdd
        ovalLayer2.lineWidth = 1
        
        catFootLayer1.addSublayer(ovalLayer2)
        
        // Oval
        //
        let ovalLayer3 = CAShapeLayer()
        ovalLayer3.name = "Oval"
        ovalLayer3.bounds = CGRect(x: 0, y: 0, width: 7.412627, height: 12.846992)
        ovalLayer3.position = CGPoint(x: 30.583055, y: 9.76809)
        ovalLayer3.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer3.contentsGravity = kCAGravityCenter
        ovalLayer3.contentsScale = 2
        ovalLayer3.path = ovalPath3
        ovalLayer3.fillColor = fillColor.cgColor
        ovalLayer3.strokeColor = strokeColor.cgColor
        ovalLayer3.fillRule = kCAFillRuleEvenOdd
        ovalLayer3.lineWidth = 1
        
        catFootLayer1.addSublayer(ovalLayer3)
        
        // Oval 2
        //
        let ovalLayer4 = CAShapeLayer()
        ovalLayer4.name = "Oval 2"
        ovalLayer4.bounds = CGRect(x: 0, y: 0, width: 23.586779, height: 18.325704)
        ovalLayer4.position = CGPoint(x: 8.463824, y: 17.172496)
        ovalLayer4.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer4.contentsGravity = kCAGravityCenter
        ovalLayer4.contentsScale = 2
        ovalLayer4.path = ovalPath4
        ovalLayer4.fillColor = fillColor.cgColor
        ovalLayer4.strokeColor = strokeColor.cgColor
        ovalLayer4.fillRule = kCAFillRuleEvenOdd
        ovalLayer4.lineWidth = 1
        
        catFootLayer1.addSublayer(ovalLayer4)
        
        catFootLayer.addSublayer(catFootLayer1)
        
        self.layer.addSublayer(catFootLayer)
        
        // CatFoot7
        //
        let catFootLayer2 = CALayer()
        catFootLayer2.name = "CatFoot7"
        catFootLayer2.bounds = CGRect(x: 0, y: 0, width: 54, height: 52)
        catFootLayer2.position = CGPoint(x: 140, y: 27)
        catFootLayer2.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer2.contentsGravity = kCAGravityCenter
        catFootLayer2.contentsScale = 2
        catFootLayer2.transform = CATransform3D( m11: 0.853034, m12: 0.521855, m13: 0, m14: 0,
                                                 m21: -0.521855, m22: 0.853034, m23: 0, m24: 0,
                                                 m31: 0, m32: 0, m33: 1, m34: 0,
                                                 m41: 0, m42: 0, m43: 0, m44: 1 )
        
        // CatFoot7 Sublayers
        //
        
        // CatFoot
        //
        let catFootLayer3 = CALayer()
        catFootLayer3.name = "CatFoot"
        catFootLayer3.bounds = CGRect(x: 0, y: 0, width: 40, height: 35.652174)
        catFootLayer3.position = CGPoint(x: 7, y: 7.754613)
        catFootLayer3.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer3.contentsGravity = kCAGravityCenter
        catFootLayer3.contentsScale = 2
        catFootLayer3.opacity = 0
        
        // CatFoot Animations
        //
        
        // opacity
        //
        let opacityAnimation1 = CAKeyframeAnimation()
        opacityAnimation1.beginTime = 6
        opacityAnimation1.duration = 1
        opacityAnimation1.fillMode = kCAFillModeForwards
        opacityAnimation1.isRemovedOnCompletion = false
        opacityAnimation1.keyPath = "opacity"
        opacityAnimation1.values = [ 0, 1 ]
        opacityAnimation1.calculationMode = kCAAnimationLinear
        
        
        // CatFoot Sublayers
        //
        
        // Oval
        //
        let ovalLayer5 = CAShapeLayer()
        ovalLayer5.name = "Oval"
        ovalLayer5.bounds = CGRect(x: 0, y: 0, width: 7.384633, height: 12.551613)
        ovalLayer5.position = CGPoint(x: 2.329651, y: 10.05005)
        ovalLayer5.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer5.contentsGravity = kCAGravityCenter
        ovalLayer5.contentsScale = 2
        ovalLayer5.path = ovalPath5
        ovalLayer5.fillColor = fillColor.cgColor
        ovalLayer5.strokeColor = strokeColor.cgColor
        ovalLayer5.fillRule = kCAFillRuleEvenOdd
        ovalLayer5.lineWidth = 1
        
        catFootLayer3.addSublayer(ovalLayer5)
        
        // Oval
        //
        let ovalLayer6 = CAShapeLayer()
        ovalLayer6.name = "Oval"
        ovalLayer6.bounds = CGRect(x: 0, y: 0, width: 7.916001, height: 14.240465)
        ovalLayer6.position = CGPoint(x: 10.139501, y: 1.110983)
        ovalLayer6.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer6.contentsGravity = kCAGravityCenter
        ovalLayer6.contentsScale = 2
        ovalLayer6.path = ovalPath6
        ovalLayer6.fillColor = fillColor.cgColor
        ovalLayer6.strokeColor = strokeColor.cgColor
        ovalLayer6.fillRule = kCAFillRuleEvenOdd
        ovalLayer6.lineWidth = 1
        
        catFootLayer3.addSublayer(ovalLayer6)
        
        // Oval
        //
        let ovalLayer7 = CAShapeLayer()
        ovalLayer7.name = "Oval"
        ovalLayer7.bounds = CGRect(x: 0, y: 0, width: 8.208792, height: 14.733717)
        ovalLayer7.position = CGPoint(x: 22.182283, y: 0.712935)
        ovalLayer7.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer7.contentsGravity = kCAGravityCenter
        ovalLayer7.contentsScale = 2
        ovalLayer7.path = ovalPath7
        ovalLayer7.fillColor = fillColor.cgColor
        ovalLayer7.strokeColor = strokeColor.cgColor
        ovalLayer7.fillRule = kCAFillRuleEvenOdd
        ovalLayer7.lineWidth = 1
        
        catFootLayer3.addSublayer(ovalLayer7)
        
        // Oval
        //
        let ovalLayer8 = CAShapeLayer()
        ovalLayer8.name = "Oval"
        ovalLayer8.bounds = CGRect(x: 0, y: 0, width: 7.412627, height: 12.846992)
        ovalLayer8.position = CGPoint(x: 30.583055, y: 9.76809)
        ovalLayer8.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer8.contentsGravity = kCAGravityCenter
        ovalLayer8.contentsScale = 2
        ovalLayer8.path = ovalPath8
        ovalLayer8.fillColor = fillColor.cgColor
        ovalLayer8.strokeColor = strokeColor.cgColor
        ovalLayer8.fillRule = kCAFillRuleEvenOdd
        ovalLayer8.lineWidth = 1
        
        catFootLayer3.addSublayer(ovalLayer8)
        
        // Oval 2
        //
        let ovalLayer9 = CAShapeLayer()
        ovalLayer9.name = "Oval 2"
        ovalLayer9.bounds = CGRect(x: 0, y: 0, width: 23.586779, height: 18.325704)
        ovalLayer9.position = CGPoint(x: 8.463824, y: 17.172496)
        ovalLayer9.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer9.contentsGravity = kCAGravityCenter
        ovalLayer9.contentsScale = 2
        ovalLayer9.path = ovalPath9
        ovalLayer9.fillColor = fillColor.cgColor
        ovalLayer9.strokeColor = strokeColor.cgColor
        ovalLayer9.fillRule = kCAFillRuleEvenOdd
        ovalLayer9.lineWidth = 1
        
        catFootLayer3.addSublayer(ovalLayer9)
        
        catFootLayer2.addSublayer(catFootLayer3)
        
        self.layer.addSublayer(catFootLayer2)
        
        // CatFoot6
        //
        let catFootLayer4 = CALayer()
        catFootLayer4.name = "CatFoot6"
        catFootLayer4.bounds = CGRect(x: 0, y: 0, width: 54, height: 52)
        catFootLayer4.position = CGPoint(x: 160, y: 92)
        catFootLayer4.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer4.contentsGravity = kCAGravityCenter
        catFootLayer4.contentsScale = 2
        catFootLayer4.transform = CATransform3D( m11: 0.824871, m12: 0.565321, m13: 0, m14: 0,
                                                 m21: -0.565321, m22: 0.824871, m23: 0, m24: 0,
                                                 m31: 0, m32: 0, m33: 1, m34: 0,
                                                 m41: 0, m42: 0, m43: 0, m44: 1 )
        
        // CatFoot6 Sublayers
        //
        
        // CatFoot
        //
        let catFootLayer5 = CALayer()
        catFootLayer5.name = "CatFoot"
        catFootLayer5.bounds = CGRect(x: 0, y: 0, width: 40, height: 35.652174)
        catFootLayer5.position = CGPoint(x: 7, y: 7.754613)
        catFootLayer5.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer5.contentsGravity = kCAGravityCenter
        catFootLayer5.contentsScale = 2
        catFootLayer5.opacity = 0
        
        // CatFoot Animations
        //
        
        // opacity
        //
        let opacityAnimation2 = CAKeyframeAnimation()
        opacityAnimation2.beginTime = 5
        opacityAnimation2.duration = 1
        opacityAnimation2.fillMode = kCAFillModeForwards
        opacityAnimation2.isRemovedOnCompletion = false
        opacityAnimation2.keyPath = "opacity"
        opacityAnimation2.values = [ 0, 1 ]
        opacityAnimation2.calculationMode = kCAAnimationLinear
        
        
        // CatFoot Sublayers
        //
        
        // Oval
        //
        let ovalLayer10 = CAShapeLayer()
        ovalLayer10.name = "Oval"
        ovalLayer10.bounds = CGRect(x: 0, y: 0, width: 7.384633, height: 12.551613)
        ovalLayer10.position = CGPoint(x: 2.329651, y: 10.05005)
        ovalLayer10.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer10.contentsGravity = kCAGravityCenter
        ovalLayer10.contentsScale = 2
        ovalLayer10.path = ovalPath10
        ovalLayer10.fillColor = fillColor.cgColor
        ovalLayer10.strokeColor = strokeColor.cgColor
        ovalLayer10.fillRule = kCAFillRuleEvenOdd
        ovalLayer10.lineWidth = 1
        
        catFootLayer5.addSublayer(ovalLayer10)
        
        // Oval
        //
        let ovalLayer11 = CAShapeLayer()
        ovalLayer11.name = "Oval"
        ovalLayer11.bounds = CGRect(x: 0, y: 0, width: 7.916001, height: 14.240465)
        ovalLayer11.position = CGPoint(x: 10.139501, y: 1.110983)
        ovalLayer11.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer11.contentsGravity = kCAGravityCenter
        ovalLayer11.contentsScale = 2
        ovalLayer11.path = ovalPath11
        ovalLayer11.fillColor = fillColor.cgColor
        ovalLayer11.strokeColor = strokeColor.cgColor
        ovalLayer11.fillRule = kCAFillRuleEvenOdd
        ovalLayer11.lineWidth = 1
        
        catFootLayer5.addSublayer(ovalLayer11)
        
        // Oval
        //
        let ovalLayer12 = CAShapeLayer()
        ovalLayer12.name = "Oval"
        ovalLayer12.bounds = CGRect(x: 0, y: 0, width: 8.208792, height: 14.733717)
        ovalLayer12.position = CGPoint(x: 22.182283, y: 0.712935)
        ovalLayer12.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer12.contentsGravity = kCAGravityCenter
        ovalLayer12.contentsScale = 2
        ovalLayer12.path = ovalPath12
        ovalLayer12.fillColor = fillColor.cgColor
        ovalLayer12.strokeColor = strokeColor.cgColor
        ovalLayer12.fillRule = kCAFillRuleEvenOdd
        ovalLayer12.lineWidth = 1
        
        catFootLayer5.addSublayer(ovalLayer12)
        
        // Oval
        //
        let ovalLayer13 = CAShapeLayer()
        ovalLayer13.name = "Oval"
        ovalLayer13.bounds = CGRect(x: 0, y: 0, width: 7.412627, height: 12.846992)
        ovalLayer13.position = CGPoint(x: 30.583055, y: 9.76809)
        ovalLayer13.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer13.contentsGravity = kCAGravityCenter
        ovalLayer13.contentsScale = 2
        ovalLayer13.path = ovalPath13
        ovalLayer13.fillColor = fillColor.cgColor
        ovalLayer13.strokeColor = strokeColor.cgColor
        ovalLayer13.fillRule = kCAFillRuleEvenOdd
        ovalLayer13.lineWidth = 1
        
        catFootLayer5.addSublayer(ovalLayer13)
        
        // Oval 2
        //
        let ovalLayer14 = CAShapeLayer()
        ovalLayer14.name = "Oval 2"
        ovalLayer14.bounds = CGRect(x: 0, y: 0, width: 23.586779, height: 18.325704)
        ovalLayer14.position = CGPoint(x: 8.463824, y: 17.172496)
        ovalLayer14.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer14.contentsGravity = kCAGravityCenter
        ovalLayer14.contentsScale = 2
        ovalLayer14.path = ovalPath14
        ovalLayer14.fillColor = fillColor.cgColor
        ovalLayer14.strokeColor = strokeColor.cgColor
        ovalLayer14.fillRule = kCAFillRuleEvenOdd
        ovalLayer14.lineWidth = 1
        
        catFootLayer5.addSublayer(ovalLayer14)
        
        catFootLayer4.addSublayer(catFootLayer5)
        
        self.layer.addSublayer(catFootLayer4)
        
        // CatFoot4
        //
        let catFootLayer6 = CALayer()
        catFootLayer6.name = "CatFoot4"
        catFootLayer6.bounds = CGRect(x: 0, y: 0, width: 54, height: 52)
        catFootLayer6.position = CGPoint(x: 37, y: -18)
        catFootLayer6.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer6.contentsGravity = kCAGravityCenter
        catFootLayer6.contentsScale = 2
        catFootLayer6.transform = CATransform3D( m11: 0.906308, m12: 0.422618, m13: 0, m14: 0,
                                                 m21: -0.422618, m22: 0.906308, m23: 0, m24: 0,
                                                 m31: 0, m32: 0, m33: 1, m34: 0,
                                                 m41: 0, m42: 0, m43: 0, m44: 1 )
        
        self.layer.addSublayer(catFootLayer6)
        
        // CatFoot8
        //
        let catFootLayer7 = CALayer()
        catFootLayer7.name = "CatFoot8"
        catFootLayer7.bounds = CGRect(x: 0, y: 0, width: 40, height: 35.652174)
        catFootLayer7.position = CGPoint(x: 213.066917, y: 37.986394)
        catFootLayer7.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer7.contentsGravity = kCAGravityCenter
        catFootLayer7.contentsScale = 2
        catFootLayer7.opacity = 0
        catFootLayer7.transform = CATransform3D( m11: 0.839259, m12: 0.543732, m13: 0, m14: 0,
                                                 m21: -0.543732, m22: 0.839259, m23: 0, m24: 0,
                                                 m31: 0, m32: 0, m33: 1, m34: 0,
                                                 m41: 0, m42: 0, m43: 0, m44: 1 )
        
        // CatFoot8 Animations
        //
        
        // opacity
        //
        let opacityAnimation3 = CAKeyframeAnimation()
        opacityAnimation3.beginTime = 7
        opacityAnimation3.duration = 1
        opacityAnimation3.fillMode = kCAFillModeForwards
        opacityAnimation3.isRemovedOnCompletion = false
        opacityAnimation3.keyPath = "opacity"
        opacityAnimation3.values = [ 0, 1 ]
        opacityAnimation3.calculationMode = kCAAnimationLinear
        
        
        // CatFoot8 Sublayers
        //
        
        // Oval
        //
        let ovalLayer15 = CAShapeLayer()
        ovalLayer15.name = "Oval"
        ovalLayer15.bounds = CGRect(x: 0, y: 0, width: 7.384633, height: 12.551613)
        ovalLayer15.position = CGPoint(x: 2.329651, y: 10.05005)
        ovalLayer15.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer15.contentsGravity = kCAGravityCenter
        ovalLayer15.contentsScale = 2
        ovalLayer15.path = ovalPath15
        ovalLayer15.fillColor = fillColor.cgColor
        ovalLayer15.strokeColor = strokeColor.cgColor
        ovalLayer15.fillRule = kCAFillRuleEvenOdd
        ovalLayer15.lineWidth = 1
        
        catFootLayer7.addSublayer(ovalLayer15)
        
        // Oval
        //
        let ovalLayer16 = CAShapeLayer()
        ovalLayer16.name = "Oval"
        ovalLayer16.bounds = CGRect(x: 0, y: 0, width: 7.916001, height: 14.240465)
        ovalLayer16.position = CGPoint(x: 10.139501, y: 1.110983)
        ovalLayer16.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer16.contentsGravity = kCAGravityCenter
        ovalLayer16.contentsScale = 2
        ovalLayer16.path = ovalPath16
        ovalLayer16.fillColor = fillColor.cgColor
        ovalLayer16.strokeColor = strokeColor.cgColor
        ovalLayer16.fillRule = kCAFillRuleEvenOdd
        ovalLayer16.lineWidth = 1
        
        catFootLayer7.addSublayer(ovalLayer16)
        
        // Oval
        //
        let ovalLayer17 = CAShapeLayer()
        ovalLayer17.name = "Oval"
        ovalLayer17.bounds = CGRect(x: 0, y: 0, width: 8.208792, height: 14.733717)
        ovalLayer17.position = CGPoint(x: 22.182283, y: 0.712935)
        ovalLayer17.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer17.contentsGravity = kCAGravityCenter
        ovalLayer17.contentsScale = 2
        ovalLayer17.path = ovalPath17
        ovalLayer17.fillColor = fillColor.cgColor
        ovalLayer17.strokeColor = strokeColor.cgColor
        ovalLayer17.fillRule = kCAFillRuleEvenOdd
        ovalLayer17.lineWidth = 1
        
        catFootLayer7.addSublayer(ovalLayer17)
        
        // Oval
        //
        let ovalLayer18 = CAShapeLayer()
        ovalLayer18.name = "Oval"
        ovalLayer18.bounds = CGRect(x: 0, y: 0, width: 7.412627, height: 12.846992)
        ovalLayer18.position = CGPoint(x: 30.583055, y: 9.76809)
        ovalLayer18.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer18.contentsGravity = kCAGravityCenter
        ovalLayer18.contentsScale = 2
        ovalLayer18.path = ovalPath18
        ovalLayer18.fillColor = fillColor.cgColor
        ovalLayer18.strokeColor = strokeColor.cgColor
        ovalLayer18.fillRule = kCAFillRuleEvenOdd
        ovalLayer18.lineWidth = 1
        
        catFootLayer7.addSublayer(ovalLayer18)
        
        // Oval 2
        //
        let ovalLayer19 = CAShapeLayer()
        ovalLayer19.name = "Oval 2"
        ovalLayer19.bounds = CGRect(x: 0, y: 0, width: 23.586779, height: 18.325704)
        ovalLayer19.position = CGPoint(x: 8.463824, y: 17.172496)
        ovalLayer19.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer19.contentsGravity = kCAGravityCenter
        ovalLayer19.contentsScale = 2
        ovalLayer19.path = ovalPath19
        ovalLayer19.fillColor = fillColor.cgColor
        ovalLayer19.strokeColor = strokeColor.cgColor
        ovalLayer19.fillRule = kCAFillRuleEvenOdd
        ovalLayer19.lineWidth = 1
        
        catFootLayer7.addSublayer(ovalLayer19)
        
        self.layer.addSublayer(catFootLayer7)
        
        // CatFoot1
        //
        let catFootLayer8 = CALayer()
        catFootLayer8.name = "CatFoot1"
        catFootLayer8.bounds = CGRect(x: 0, y: 0, width: 54, height: 52)
        catFootLayer8.position = CGPoint(x: 7.621081, y: 228.887519)
        catFootLayer8.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer8.contentsGravity = kCAGravityCenter
        catFootLayer8.contentsScale = 2
        catFootLayer8.transform = CATransform3D( m11: 0.874257, m12: 0.485464, m13: 0, m14: 0,
                                                 m21: -0.485464, m22: 0.874257, m23: 0, m24: 0,
                                                 m31: 0, m32: 0, m33: 1, m34: 0,
                                                 m41: 0, m42: 0, m43: 0, m44: 1 )
        
        // CatFoot1 Sublayers
        //
        
        // CatFoot
        //
        let catFootLayer9 = CALayer()
        catFootLayer9.name = "CatFoot"
        catFootLayer9.bounds = CGRect(x: 0, y: 0, width: 40, height: 35.652174)
        catFootLayer9.position = CGPoint(x: 7, y: 7.754613)
        catFootLayer9.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer9.contentsGravity = kCAGravityCenter
        catFootLayer9.contentsScale = 2
        catFootLayer9.opacity = 0
        
        // CatFoot Animations
        //
        
        // opacity
        //
        let opacityAnimation4 = CAKeyframeAnimation()
        opacityAnimation4.beginTime = -0
        opacityAnimation4.duration = 1
        opacityAnimation4.fillMode = kCAFillModeForwards
        opacityAnimation4.isRemovedOnCompletion = false
        opacityAnimation4.keyPath = "opacity"
        opacityAnimation4.values = [ 0, 1 ]
        opacityAnimation4.calculationMode = kCAAnimationLinear
        
        
        // CatFoot Sublayers
        //
        
        // Oval
        //
        let ovalLayer20 = CAShapeLayer()
        ovalLayer20.name = "Oval"
        ovalLayer20.bounds = CGRect(x: 0, y: 0, width: 7.384633, height: 12.551613)
        ovalLayer20.position = CGPoint(x: 2.329651, y: 10.05005)
        ovalLayer20.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer20.contentsGravity = kCAGravityCenter
        ovalLayer20.contentsScale = 2
        ovalLayer20.path = ovalPath20
        ovalLayer20.fillColor = fillColor.cgColor
        ovalLayer20.strokeColor = strokeColor.cgColor
        ovalLayer20.fillRule = kCAFillRuleEvenOdd
        ovalLayer20.lineWidth = 1
        
        catFootLayer9.addSublayer(ovalLayer20)
        
        // Oval
        //
        let ovalLayer21 = CAShapeLayer()
        ovalLayer21.name = "Oval"
        ovalLayer21.bounds = CGRect(x: 0, y: 0, width: 7.916001, height: 14.240465)
        ovalLayer21.position = CGPoint(x: 10.139501, y: 1.110983)
        ovalLayer21.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer21.contentsGravity = kCAGravityCenter
        ovalLayer21.contentsScale = 2
        ovalLayer21.path = ovalPath21
        ovalLayer21.fillColor = fillColor.cgColor
        ovalLayer21.strokeColor = strokeColor.cgColor
        ovalLayer21.fillRule = kCAFillRuleEvenOdd
        ovalLayer21.lineWidth = 1
        
        catFootLayer9.addSublayer(ovalLayer21)
        
        // Oval
        //
        let ovalLayer22 = CAShapeLayer()
        ovalLayer22.name = "Oval"
        ovalLayer22.bounds = CGRect(x: 0, y: 0, width: 8.208792, height: 14.733717)
        ovalLayer22.position = CGPoint(x: 22.182283, y: 0.712935)
        ovalLayer22.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer22.contentsGravity = kCAGravityCenter
        ovalLayer22.contentsScale = 2
        ovalLayer22.path = ovalPath22
        ovalLayer22.fillColor = fillColor.cgColor
        ovalLayer22.strokeColor = strokeColor.cgColor
        ovalLayer22.fillRule = kCAFillRuleEvenOdd
        ovalLayer22.lineWidth = 1
        
        catFootLayer9.addSublayer(ovalLayer22)
        
        // Oval
        //
        let ovalLayer23 = CAShapeLayer()
        ovalLayer23.name = "Oval"
        ovalLayer23.bounds = CGRect(x: 0, y: 0, width: 7.412627, height: 12.846992)
        ovalLayer23.position = CGPoint(x: 30.583055, y: 9.76809)
        ovalLayer23.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer23.contentsGravity = kCAGravityCenter
        ovalLayer23.contentsScale = 2
        ovalLayer23.path = ovalPath23
        ovalLayer23.fillColor = fillColor.cgColor
        ovalLayer23.strokeColor = strokeColor.cgColor
        ovalLayer23.fillRule = kCAFillRuleEvenOdd
        ovalLayer23.lineWidth = 1
        
        catFootLayer9.addSublayer(ovalLayer23)
        
        // Oval 2
        //
        let ovalLayer24 = CAShapeLayer()
        ovalLayer24.name = "Oval 2"
        ovalLayer24.bounds = CGRect(x: 0, y: 0, width: 23.586779, height: 18.325704)
        ovalLayer24.position = CGPoint(x: 8.463824, y: 17.172496)
        ovalLayer24.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer24.contentsGravity = kCAGravityCenter
        ovalLayer24.contentsScale = 2
        ovalLayer24.path = ovalPath24
        ovalLayer24.fillColor = fillColor.cgColor
        ovalLayer24.strokeColor = strokeColor.cgColor
        ovalLayer24.fillRule = kCAFillRuleEvenOdd
        ovalLayer24.lineWidth = 1
        
        catFootLayer9.addSublayer(ovalLayer24)
        
        catFootLayer8.addSublayer(catFootLayer9)
        
        self.layer.addSublayer(catFootLayer8)
        
        // CatFoot2
        //
        let catFootLayer10 = CALayer()
        catFootLayer10.name = "CatFoot2"
        catFootLayer10.bounds = CGRect(x: 0, y: 0, width: 54, height: 52)
        catFootLayer10.position = CGPoint(x: 73.868369, y: 211.918587)
        catFootLayer10.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer10.contentsGravity = kCAGravityCenter
        catFootLayer10.contentsScale = 2
        catFootLayer10.transform = CATransform3D( m11: 0.859247, m12: 0.511561, m13: 0, m14: 0,
                                                  m21: -0.511561, m22: 0.859247, m23: 0, m24: 0,
                                                  m31: 0, m32: 0, m33: 1, m34: 0,
                                                  m41: 0, m42: 0, m43: 0, m44: 1 )
        
        // CatFoot2 Sublayers
        //
        
        // CatFoot
        //
        let catFootLayer11 = CALayer()
        catFootLayer11.name = "CatFoot"
        catFootLayer11.bounds = CGRect(x: 0, y: 0, width: 40, height: 35.652174)
        catFootLayer11.position = CGPoint(x: 7, y: 7.754613)
        catFootLayer11.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer11.contentsGravity = kCAGravityCenter
        catFootLayer11.contentsScale = 2
        catFootLayer11.opacity = 0
        
        // CatFoot Animations
        //
        
        // opacity
        //
        let opacityAnimation5 = CAKeyframeAnimation()
        opacityAnimation5.beginTime = 1.000001
        opacityAnimation5.duration = 1
        opacityAnimation5.fillMode = kCAFillModeForwards
        opacityAnimation5.isRemovedOnCompletion = false
        opacityAnimation5.keyPath = "opacity"
        opacityAnimation5.values = [ 0, 1 ]
        opacityAnimation5.calculationMode = kCAAnimationLinear
        
        
        // CatFoot Sublayers
        //
        
        // Oval
        //
        let ovalLayer25 = CAShapeLayer()
        ovalLayer25.name = "Oval"
        ovalLayer25.bounds = CGRect(x: 0, y: 0, width: 7.384633, height: 12.551613)
        ovalLayer25.position = CGPoint(x: 2.329651, y: 10.05005)
        ovalLayer25.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer25.contentsGravity = kCAGravityCenter
        ovalLayer25.contentsScale = 2
        ovalLayer25.path = ovalPath25
        ovalLayer25.fillColor = fillColor.cgColor
        ovalLayer25.strokeColor = strokeColor.cgColor
        ovalLayer25.fillRule = kCAFillRuleEvenOdd
        ovalLayer25.lineWidth = 1
        
        catFootLayer11.addSublayer(ovalLayer25)
        
        // Oval
        //
        let ovalLayer26 = CAShapeLayer()
        ovalLayer26.name = "Oval"
        ovalLayer26.bounds = CGRect(x: 0, y: 0, width: 7.916001, height: 14.240465)
        ovalLayer26.position = CGPoint(x: 10.139501, y: 1.110983)
        ovalLayer26.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer26.contentsGravity = kCAGravityCenter
        ovalLayer26.contentsScale = 2
        ovalLayer26.path = ovalPath26
        ovalLayer26.fillColor = fillColor.cgColor
        ovalLayer26.strokeColor = strokeColor.cgColor
        ovalLayer26.fillRule = kCAFillRuleEvenOdd
        ovalLayer26.lineWidth = 1
        
        catFootLayer11.addSublayer(ovalLayer26)
        
        // Oval
        //
        let ovalLayer27 = CAShapeLayer()
        ovalLayer27.name = "Oval"
        ovalLayer27.bounds = CGRect(x: 0, y: 0, width: 8.208792, height: 14.733717)
        ovalLayer27.position = CGPoint(x: 22.182283, y: 0.712935)
        ovalLayer27.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer27.contentsGravity = kCAGravityCenter
        ovalLayer27.contentsScale = 2
        ovalLayer27.path = ovalPath27
        ovalLayer27.fillColor = fillColor.cgColor
        ovalLayer27.strokeColor = strokeColor.cgColor
        ovalLayer27.fillRule = kCAFillRuleEvenOdd
        ovalLayer27.lineWidth = 1
        
        catFootLayer11.addSublayer(ovalLayer27)
        
        // Oval
        //
        let ovalLayer28 = CAShapeLayer()
        ovalLayer28.name = "Oval"
        ovalLayer28.bounds = CGRect(x: 0, y: 0, width: 7.412627, height: 12.846992)
        ovalLayer28.position = CGPoint(x: 30.583055, y: 9.76809)
        ovalLayer28.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer28.contentsGravity = kCAGravityCenter
        ovalLayer28.contentsScale = 2
        ovalLayer28.path = ovalPath28
        ovalLayer28.fillColor = fillColor.cgColor
        ovalLayer28.strokeColor = strokeColor.cgColor
        ovalLayer28.fillRule = kCAFillRuleEvenOdd
        ovalLayer28.lineWidth = 1
        
        catFootLayer11.addSublayer(ovalLayer28)
        
        // Oval 2
        //
        let ovalLayer29 = CAShapeLayer()
        ovalLayer29.name = "Oval 2"
        ovalLayer29.bounds = CGRect(x: 0, y: 0, width: 23.586779, height: 18.325704)
        ovalLayer29.position = CGPoint(x: 8.463824, y: 17.172496)
        ovalLayer29.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer29.contentsGravity = kCAGravityCenter
        ovalLayer29.contentsScale = 2
        ovalLayer29.path = ovalPath29
        ovalLayer29.fillColor = fillColor.cgColor
        ovalLayer29.strokeColor = strokeColor.cgColor
        ovalLayer29.fillRule = kCAFillRuleEvenOdd
        ovalLayer29.lineWidth = 1
        
        catFootLayer11.addSublayer(ovalLayer29)
        
        catFootLayer10.addSublayer(catFootLayer11)
        
        self.layer.addSublayer(catFootLayer10)
        
        // CatFoot3
        //
        let catFootLayer12 = CALayer()
        catFootLayer12.name = "CatFoot3"
        catFootLayer12.bounds = CGRect(x: 0, y: 0, width: 54, height: 52)
        catFootLayer12.position = CGPoint(x: 56.202747, y: 152.624557)
        catFootLayer12.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer12.contentsGravity = kCAGravityCenter
        catFootLayer12.contentsScale = 2
        catFootLayer12.transform = CATransform3D( m11: 0.81869, m12: 0.574236, m13: 0, m14: 0,
                                                  m21: -0.574236, m22: 0.81869, m23: 0, m24: 0,
                                                  m31: 0, m32: 0, m33: 1, m34: 0,
                                                  m41: 0, m42: 0, m43: 0, m44: 1 )
        
        // CatFoot3 Sublayers
        //
        
        // CatFoot
        //
        let catFootLayer13 = CALayer()
        catFootLayer13.name = "CatFoot"
        catFootLayer13.bounds = CGRect(x: 0, y: 0, width: 40, height: 35.652174)
        catFootLayer13.position = CGPoint(x: 7, y: 7.754613)
        catFootLayer13.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer13.contentsGravity = kCAGravityCenter
        catFootLayer13.contentsScale = 2
        catFootLayer13.opacity = 0
        
        // CatFoot Animations
        //
        
        // opacity
        //
        let opacityAnimation6 = CAKeyframeAnimation()
        opacityAnimation6.beginTime = 2
        opacityAnimation6.duration = 1
        opacityAnimation6.fillMode = kCAFillModeForwards
        opacityAnimation6.isRemovedOnCompletion = false
        opacityAnimation6.keyPath = "opacity"
        opacityAnimation6.values = [ 0, 1 ]
        opacityAnimation6.calculationMode = kCAAnimationLinear
        
        
        // CatFoot Sublayers
        //
        
        // Oval
        //
        let ovalLayer30 = CAShapeLayer()
        ovalLayer30.name = "Oval"
        ovalLayer30.bounds = CGRect(x: 0, y: 0, width: 7.384633, height: 12.551613)
        ovalLayer30.position = CGPoint(x: 2.329651, y: 10.05005)
        ovalLayer30.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer30.contentsGravity = kCAGravityCenter
        ovalLayer30.contentsScale = 2
        ovalLayer30.path = ovalPath30
        ovalLayer30.fillColor = fillColor.cgColor
        ovalLayer30.strokeColor = strokeColor.cgColor
        ovalLayer30.fillRule = kCAFillRuleEvenOdd
        ovalLayer30.lineWidth = 1
        
        catFootLayer13.addSublayer(ovalLayer30)
        
        // Oval
        //
        let ovalLayer31 = CAShapeLayer()
        ovalLayer31.name = "Oval"
        ovalLayer31.bounds = CGRect(x: 0, y: 0, width: 7.916001, height: 14.240465)
        ovalLayer31.position = CGPoint(x: 10.139501, y: 1.110983)
        ovalLayer31.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer31.contentsGravity = kCAGravityCenter
        ovalLayer31.contentsScale = 2
        ovalLayer31.path = ovalPath31
        ovalLayer31.fillColor = fillColor.cgColor
        ovalLayer31.strokeColor = strokeColor.cgColor
        ovalLayer31.fillRule = kCAFillRuleEvenOdd
        ovalLayer31.lineWidth = 1
        
        catFootLayer13.addSublayer(ovalLayer31)
        
        // Oval
        //
        let ovalLayer32 = CAShapeLayer()
        ovalLayer32.name = "Oval"
        ovalLayer32.bounds = CGRect(x: 0, y: 0, width: 8.208792, height: 14.733717)
        ovalLayer32.position = CGPoint(x: 22.182283, y: 0.712935)
        ovalLayer32.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer32.contentsGravity = kCAGravityCenter
        ovalLayer32.contentsScale = 2
        ovalLayer32.path = ovalPath32
        ovalLayer32.fillColor = fillColor.cgColor
        ovalLayer32.strokeColor = strokeColor.cgColor
        ovalLayer32.fillRule = kCAFillRuleEvenOdd
        ovalLayer32.lineWidth = 1
        
        catFootLayer13.addSublayer(ovalLayer32)
        
        // Oval
        //
        let ovalLayer33 = CAShapeLayer()
        ovalLayer33.name = "Oval"
        ovalLayer33.bounds = CGRect(x: 0, y: 0, width: 7.412627, height: 12.846992)
        ovalLayer33.position = CGPoint(x: 30.583055, y: 9.76809)
        ovalLayer33.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer33.contentsGravity = kCAGravityCenter
        ovalLayer33.contentsScale = 2
        ovalLayer33.path = ovalPath33
        ovalLayer33.fillColor = fillColor.cgColor
        ovalLayer33.strokeColor = strokeColor.cgColor
        ovalLayer33.fillRule = kCAFillRuleEvenOdd
        ovalLayer33.lineWidth = 1
        
        catFootLayer13.addSublayer(ovalLayer33)
        
        // Oval 2
        //
        let ovalLayer34 = CAShapeLayer()
        ovalLayer34.name = "Oval 2"
        ovalLayer34.bounds = CGRect(x: 0, y: 0, width: 23.586779, height: 18.325704)
        ovalLayer34.position = CGPoint(x: 8.463824, y: 17.172496)
        ovalLayer34.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer34.contentsGravity = kCAGravityCenter
        ovalLayer34.contentsScale = 2
        ovalLayer34.path = ovalPath34
        ovalLayer34.fillColor = fillColor.cgColor
        ovalLayer34.strokeColor = strokeColor.cgColor
        ovalLayer34.fillRule = kCAFillRuleEvenOdd
        ovalLayer34.lineWidth = 1
        
        catFootLayer13.addSublayer(ovalLayer34)
        
        catFootLayer12.addSublayer(catFootLayer13)
        
        self.layer.addSublayer(catFootLayer12)
        
        // CatFoot4
        //
        let catFootLayer14 = CALayer()
        catFootLayer14.name = "CatFoot4"
        catFootLayer14.bounds = CGRect(x: 0, y: 0, width: 54, height: 52)
        catFootLayer14.position = CGPoint(x: 125.298867, y: 144.508894)
        catFootLayer14.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer14.contentsGravity = kCAGravityCenter
        catFootLayer14.contentsScale = 2
        catFootLayer14.transform = CATransform3D( m11: 0.819143, m12: 0.57359, m13: 0, m14: 0,
                                                  m21: -0.57359, m22: 0.819143, m23: 0, m24: 0,
                                                  m31: 0, m32: 0, m33: 1, m34: 0,
                                                  m41: 0, m42: 0, m43: 0, m44: 1 )
        
        // CatFoot4 Sublayers
        //
        
        // CatFoot
        //
        let catFootLayer15 = CALayer()
        catFootLayer15.name = "CatFoot"
        catFootLayer15.bounds = CGRect(x: 0, y: 0, width: 40, height: 35.652174)
        catFootLayer15.position = CGPoint(x: 7, y: 7.754613)
        catFootLayer15.anchorPoint = CGPoint(x: 0, y: 0)
        catFootLayer15.contentsGravity = kCAGravityCenter
        catFootLayer15.contentsScale = 2
        catFootLayer15.opacity = 0
        
        // CatFoot Animations
        //
        
        // opacity
        //
        let opacityAnimation7 = CAKeyframeAnimation()
        opacityAnimation7.beginTime = 3
        opacityAnimation7.duration = 1
        opacityAnimation7.fillMode = kCAFillModeForwards
        opacityAnimation7.isRemovedOnCompletion = false
        opacityAnimation7.keyPath = "opacity"
        opacityAnimation7.values = [ 0, 1 ]
        opacityAnimation7.calculationMode = kCAAnimationLinear
        
        
        // CatFoot Sublayers
        //
        
        // Oval
        //
        let ovalLayer35 = CAShapeLayer()
        ovalLayer35.name = "Oval"
        ovalLayer35.bounds = CGRect(x: 0, y: 0, width: 7.384633, height: 12.551613)
        ovalLayer35.position = CGPoint(x: 2.329651, y: 10.05005)
        ovalLayer35.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer35.contentsGravity = kCAGravityCenter
        ovalLayer35.contentsScale = 2
        ovalLayer35.path = ovalPath35
        ovalLayer35.fillColor = fillColor.cgColor
        ovalLayer35.strokeColor = strokeColor.cgColor
        ovalLayer35.fillRule = kCAFillRuleEvenOdd
        ovalLayer35.lineWidth = 1
        
        catFootLayer15.addSublayer(ovalLayer35)
        
        // Oval
        //
        let ovalLayer36 = CAShapeLayer()
        ovalLayer36.name = "Oval"
        ovalLayer36.bounds = CGRect(x: 0, y: 0, width: 7.916001, height: 14.240465)
        ovalLayer36.position = CGPoint(x: 10.139501, y: 1.110983)
        ovalLayer36.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer36.contentsGravity = kCAGravityCenter
        ovalLayer36.contentsScale = 2
        ovalLayer36.path = ovalPath36
        ovalLayer36.fillColor = fillColor.cgColor
        ovalLayer36.strokeColor = strokeColor.cgColor
        ovalLayer36.fillRule = kCAFillRuleEvenOdd
        ovalLayer36.lineWidth = 1
        
        catFootLayer15.addSublayer(ovalLayer36)
        
        // Oval
        //
        let ovalLayer37 = CAShapeLayer()
        ovalLayer37.name = "Oval"
        ovalLayer37.bounds = CGRect(x: 0, y: 0, width: 8.208792, height: 14.733717)
        ovalLayer37.position = CGPoint(x: 22.182283, y: 0.712935)
        ovalLayer37.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer37.contentsGravity = kCAGravityCenter
        ovalLayer37.contentsScale = 2
        ovalLayer37.path = ovalPath37
        ovalLayer37.fillColor = fillColor.cgColor
        ovalLayer37.strokeColor = strokeColor.cgColor
        ovalLayer37.fillRule = kCAFillRuleEvenOdd
        ovalLayer37.lineWidth = 1
        
        catFootLayer15.addSublayer(ovalLayer37)
        
        // Oval
        //
        let ovalLayer38 = CAShapeLayer()
        ovalLayer38.name = "Oval"
        ovalLayer38.bounds = CGRect(x: 0, y: 0, width: 7.412627, height: 12.846992)
        ovalLayer38.position = CGPoint(x: 30.583055, y: 9.76809)
        ovalLayer38.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer38.contentsGravity = kCAGravityCenter
        ovalLayer38.contentsScale = 2
        ovalLayer38.path = ovalPath38
        ovalLayer38.fillColor = fillColor.cgColor
        ovalLayer38.strokeColor = strokeColor.cgColor
        ovalLayer38.fillRule = kCAFillRuleEvenOdd
        ovalLayer38.lineWidth = 1
        
        catFootLayer15.addSublayer(ovalLayer38)
        
        // Oval 2
        //
        let ovalLayer39 = CAShapeLayer()
        ovalLayer39.name = "Oval 2"
        ovalLayer39.bounds = CGRect(x: 0, y: 0, width: 23.586779, height: 18.325704)
        ovalLayer39.position = CGPoint(x: 8.463824, y: 17.172496)
        ovalLayer39.anchorPoint = CGPoint(x: 0, y: 0)
        ovalLayer39.contentsGravity = kCAGravityCenter
        ovalLayer39.contentsScale = 2
        ovalLayer39.path = ovalPath39
        ovalLayer39.fillColor = fillColor.cgColor
        ovalLayer39.strokeColor = strokeColor.cgColor
        ovalLayer39.fillRule = kCAFillRuleEvenOdd
        ovalLayer39.lineWidth = 1
        
        catFootLayer15.addSublayer(ovalLayer39)
        
        catFootLayer14.addSublayer(catFootLayer15)
        
        self.layer.addSublayer(catFootLayer14)
        
        // Layer
        //
        let layerLayer = CALayer()
        layerLayer.name = "Layer"
        layerLayer.bounds = CGRect(x: 0, y: 0, width: 10, height: 10)
        layerLayer.position = CGPoint(x: 0, y: 1)
        layerLayer.anchorPoint = CGPoint(x: 0, y: 0)
        layerLayer.contentsGravity = kCAGravityCenter
        layerLayer.backgroundColor = backgroundColor.cgColor
        layerLayer.borderWidth = 1
        layerLayer.borderColor = borderColor.cgColor
        layerLayer.shadowOffset = CGSize(width: 0, height: 1)
        layerLayer.fillMode = kCAFillModeForwards
        
        self.layer.addSublayer(layerLayer)
        
        // Layer Instance Assignments
        //
        self.catFootLayer9 = catFootLayer9
        self.catFootLayer11 = catFootLayer11
        self.catFootLayer13 = catFootLayer13
        self.catFootLayer15 = catFootLayer15
        self.catFootLayer1 = catFootLayer1
        self.catFootLayer5 = catFootLayer5
        self.catFootLayer3 = catFootLayer3
        self.catFootLayer7 = catFootLayer7
        
        // Animation Instance Assignments
        //
        self.opacityAnimation = opacityAnimation
        self.opacityAnimation1 = opacityAnimation1
        self.opacityAnimation2 = opacityAnimation2
        self.opacityAnimation3 = opacityAnimation3
        self.opacityAnimation4 = opacityAnimation4
        self.opacityAnimation5 = opacityAnimation5
        self.opacityAnimation6 = opacityAnimation6
        self.opacityAnimation7 = opacityAnimation7
        
    }
    
    // MARK: - Responder
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        guard let location = touches.first?.location(in: self.superview),
            let hitLayer = self.layer.presentation()?.hitTest(location) else { return }
        
        print("Layer \(hitLayer.name ?? String(describing: hitLayer)) was tapped.")
        
        // Call action methods
        //
        self.timeIntervalPlayAnimationsAction(self)
    }
    
    // MARK: - Actions
    
    @IBAction func timeIntervalPlayAnimationsAction(_ sender: Any?)
    {
        self.opacityAnimation4!.beginTime = self.catFootLayer9!.convertTime(CACurrentMediaTime(), from: nil) + -0
        self.catFootLayer9?.add(self.opacityAnimation4!, forKey: "opacityAnimation4")
        
        self.opacityAnimation5!.beginTime = self.catFootLayer11!.convertTime(CACurrentMediaTime(), from: nil) + 1.100001
        self.catFootLayer11?.add(self.opacityAnimation5!, forKey: "opacityAnimation5")
        
        self.opacityAnimation6!.beginTime = self.catFootLayer13!.convertTime(CACurrentMediaTime(), from: nil) + 2.2
        self.catFootLayer13?.add(self.opacityAnimation6!, forKey: "opacityAnimation6")
        
        self.opacityAnimation7!.beginTime = self.catFootLayer15!.convertTime(CACurrentMediaTime(), from: nil) + 3.3
        self.catFootLayer15?.add(self.opacityAnimation7!, forKey: "opacityAnimation7")
        
        self.opacityAnimation!.beginTime = self.catFootLayer1!.convertTime(CACurrentMediaTime(), from: nil) + 4.4
        self.catFootLayer1?.add(self.opacityAnimation!, forKey: "opacityAnimation")
        
        self.opacityAnimation2!.beginTime = self.catFootLayer5!.convertTime(CACurrentMediaTime(), from: nil) + 5.5
        self.catFootLayer5?.add(self.opacityAnimation2!, forKey: "opacityAnimation2")
        
        self.opacityAnimation1!.beginTime = self.catFootLayer3!.convertTime(CACurrentMediaTime(), from: nil) + 6.6
        self.catFootLayer3?.add(self.opacityAnimation1!, forKey: "opacityAnimation1")
        
        self.opacityAnimation3!.beginTime = self.catFootLayer7!.convertTime(CACurrentMediaTime(), from: nil) + 7.7
        self.catFootLayer7?.add(self.opacityAnimation3!, forKey: "opacityAnimation3")
    }
}

