//
//  NetworkApiService.swift
//  CryptoKitty
//
//  Created by Денис on 14.12.2017.
//  Copyright © 2017 Denis. All rights reserved.
//
import Foundation

class NetworkApiService {
    
   enum TypeKittie {
        case sale
        case sire
    }
    
    enum AuctionStatus {
        case open
        case close
    }
    
    // getAllKitties
    static func getAllKitties(limit: Int, offset: UInt8, generation: Int, completionHandler: @escaping (AllKitties?, Error?) -> ()) {
        sendRequestAllKitties(limit: limit, offset: offset, generation: generation, completionHandler: completionHandler)
    }
    fileprivate static func sendRequestAllKitties(limit: Int, offset: UInt8, generation: Int, completionHandler: @escaping (AllKitties?, Error?) -> ()) {
        let url = URL(string: "https://api.cryptokitties.co/kitties?limit=\(limit)&offset=\(offset)&generation=\(generation)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let result = try JSONDecoder().decode(AllKitties.self, from: data)
                completionHandler(result, error)
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    
    //getAuctions
    //type = "sale" | "sire"
    //status = "open" | "closed"
    //limit = 1-100
    //offset = INTEGER
    
    static func getAllAuctions(type: TypeKittie, status: AuctionStatus, limit: Int, offset: UInt8, completionHandler: @escaping (AllAuctions?, Error?) -> ()) {
        sendRequestAllAuctions(type: type, status: status, limit: limit, offset: offset, completionHandler: completionHandler)
    }
     static func sendRequestAllAuctions(type: TypeKittie, status: AuctionStatus, limit: Int, offset: UInt8, completionHandler: @escaping (AllAuctions?, Error?) -> ()) {
        let url = URL(string: "https://api.cryptokitties.co/auctions?type=\(type)&status=\(status)&limit=\(limit)&offset=\(offset)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let result = try JSONDecoder().decode(AllAuctions.self, from: data)
                completionHandler(result, error)
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    //getCattributes
    //total = "true" | "false"
    static func getAllCattributes(completionHandler: @escaping (Cattributes?, Error?) -> ()) {
        sendRequestAllCattributes(total: true, completionHandler: completionHandler)
    }
    fileprivate static func sendRequestAllCattributes(total: Bool, completionHandler: @escaping (Cattributes?, Error?) -> ()) {
        let url = URL(string: "https://api.cryptokitties.co/cattributes?total=\(total))")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let result = try JSONDecoder().decode(Cattributes.self, from: data)
                completionHandler(result, error)
            } catch let error {
                print(error)
            }
            }.resume()
    }
    
    
    //getKitten Kitten
    static func getKitten(completionHandler: @escaping (Kitten?, Error?) -> ()) {
        sendRequestKittenInfo(kittenId: 222, completionHandler: completionHandler)
    }
    fileprivate static func sendRequestKittenInfo(kittenId: Int, completionHandler: @escaping (Kitten?, Error?) -> ()) {
        let url = URL(string: "https://api.cryptokitties.co/kitties/\(kittenId)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let result = try JSONDecoder().decode(Kitten.self, from: data)
                completionHandler(result, error)
            } catch let error {
                print(error)
            }
            }.resume()
        
    }
    
    
    //getUserKitties
    static func getUserKitties(completionHandler: @escaping (UserKitties?, Error?) -> ()) {
        sendRequestUserKittiesInfo(ownerWalletAddress: "0xc5e38233cc0d7cff9340e6139367aba498ec9b18", limit: 10, offset: 0, completionHandler: completionHandler)
    }
     static func sendRequestUserKittiesInfo(ownerWalletAddress: String, limit: Int, offset: Int, completionHandler: @escaping (UserKitties?, Error?) -> ()) {
        let url = URL(string: "https://api.cryptokitties.co/kitties?owner_wallet_address=\(ownerWalletAddress)&limit=\(limit)&offset=\(offset)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else { return }
            guard error == nil else { return }
            do {
                let result = try JSONDecoder().decode(UserKitties.self, from: data)
                completionHandler(result, error)
            } catch let error {
                print(error)
        }
    }.resume()
    }
}









