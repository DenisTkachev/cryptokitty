//
//  FilterSettingsMO.swift
//  cats
//
//  Created by Денис on 11.03.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

import Foundation
import CoreData

@objc(FilterSettings)
class FilterSettingsMO: NSManagedObject {

    @NSManaged var sortBy: String
    @NSManaged var sortDirection: Bool
    @NSManaged var is_fancy: Bool
    @NSManaged var is_exclusive: Bool
    @NSManaged var coolDown: String
    
}

extension FilterSettingsMO {
    func fillFrom(obj: Auctions) {
        //self.current_price = obj.current_price!
    }
    
    static func insert(obj: Auctions) {
        //CoreDataHandler.saveFilterSettings(value: obj.)
    }
}
