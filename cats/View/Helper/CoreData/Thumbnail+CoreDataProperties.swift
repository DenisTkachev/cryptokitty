

import Foundation
import CoreData

extension Thumbnail {

    @NSManaged var imageData: Data?
    @NSManaged var id: NSNumber?
    @NSManaged var fullRes: NSManagedObject?

}
