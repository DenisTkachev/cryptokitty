

import Foundation
import CoreData

extension FullRes {

    @NSManaged var imageData: Data?
    @NSManaged var thumbnail: Thumbnail?

}
