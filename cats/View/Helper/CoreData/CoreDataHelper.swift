//
//  CoreDataHelper.swift
//  cats
//
//  Created by Денис on 14.02.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHandler: NSObject {

    private class func getContext() -> NSManagedObjectContext {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            return appDelegate.persistentContainer.viewContext
    }
    
    class func saveSetting(value: String) {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Settings", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(value, forKey: "countOfCats")
        
        do {
            try context.save()
            //return true
        }catch {
            //return false
            print("Error CoreDataHelper")
        }
    }
    
    class func fetchObject() -> Settings? {
        let context = getContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Settings")
        let settings: [Settings]? = nil
        do {
            guard let settings = try context.fetch(request) as? [Settings] else { return nil }
            return settings.last
        } catch {
            return settings?.last
        }
    }
    
    class func fetchFilterSettingsObject() -> FilterSettingsMO? {
        let context = getContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FilterSettings")
        let settings: [FilterSettingsMO]? = nil
        do {
            guard let settings = try context.fetch(request) as? [FilterSettingsMO] else { return nil }
            return settings.last
        } catch {
            return settings?.last
        }
    }
    
    class func saveFilterSettings(value: FilterSettings) { // передать сюда свойста фильтра
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "FilterSettings", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        manageObject.setValue(value.is_fancy, forKey: "is_fancy")
        manageObject.setValue(value.is_exclusive, forKey: "is_exclusive")
        manageObject.setValue(value.coolDown, forKey: "coolDown")
        manageObject.setValue(value.sortBy, forKey: "sortBy")
        manageObject.setValue(value.sortDirection, forKey: "sortDirection")
        do {
            try context.save()
            //return true
        }catch {
            //return false
            print("Error CoreDataHelper")
        }
    }
    
    class func saveFilterSettings(fillForm: FilterSettings) {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "FilterSettings", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(fillForm.is_fancy, forKey: "is_fancy")
        manageObject.setValue(fillForm.is_exclusive, forKey: "is_exclusive")
        manageObject.setValue(fillForm.coolDown, forKey: "coolDown")
        manageObject.setValue(fillForm.sortBy, forKey: "sortBy")
        manageObject.setValue(fillForm.sortDirection, forKey: "sortDirection")
        
        do {
            try context.save()
            //return true
        }catch {
            //return false
            print("Error CoreDataHelper")
        }
    }
//    class func deleteObject(user: Settings) -> Bool {
//
//        let context = getContext()
//        context.delete(user)
//
//        do {
//            try context.save()
//            return true
//        }catch {
//            return false
//        }
//    }
    
//
//    class func cleanDelete() -> Bool {
//        let context = getContext()
//        let delete = NSBatchDeleteRequest(fetchRequest: User.fetchRequest())
//        
//        do {
//            try context.execute(delete)
//            return true
//        }catch {
//            return false
//        }
//    }
    
    //MARK: - Video Part 3
//    class func filterData() -> [User]? {
//        let context = getContext()
//        let fetchRequest:NSFetchRequest<User> = User.fetchRequest()
//        var user:[User]? = nil
//
//        let predicate = NSPredicate(format: "password contains[c] %@", "2")
//        fetchRequest.predicate = predicate
//
//        do {
//            user = try context.fetch(fetchRequest)
//            return user
//
//        }catch {
//            return user
//        }
//    }
}









