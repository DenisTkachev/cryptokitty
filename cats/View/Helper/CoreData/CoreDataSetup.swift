

import Foundation
import CoreData
import UIKit


extension ViewController {
    
    /**
     Start Core Data managed context on the correct queue
     */
    func coreDataSetup() {
        Run.sync(coreDataQueue) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            self.managedContext = appDelegate.persistentContainer.viewContext
        }
    }
}
