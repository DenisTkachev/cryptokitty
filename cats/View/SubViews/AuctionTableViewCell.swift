//
//  AuctionTableViewCell.swift
//  cats
//
//  Created by Денис on 21.12.2017.
//  Copyright © 2017 Denis. All rights reserved.
//

import UIKit

class AuctionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var startPriceLabel: UILabel!
    @IBOutlet weak var currentPriceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var imageCat: UIImageView!
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genLabel: UILabel!
    @IBOutlet weak var colorKitLabel: UILabel!
    @IBOutlet weak var fancyLabel: UILabel!
    @IBOutlet weak var exlusiveLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        imageCat.image = nil
        idLabel.text = nil
    }
    
    func configure(_ auctions: [Auctions], indexPath: IndexPath) {
        
        if let kitty = auctions[indexPath.row].kitty {
            colorKitLabel.text = kitty.color?.uppercased()
            idLabel.text = String(describing: kitty.id!)
            nameLabel.text = kitty.name
            genLabel.text = String(describing: kitty.generation!)
            fancyLabel.text = kitty.fancy_type
            exlusiveLabel.text = (kitty.is_exclusive == true)  ? "Yes" : "No"
        }
        
        if let seller = auctions[indexPath.row].seller {
            sellerNameLabel.text = seller.nickname
        }
        
        let auction = auctions[indexPath.row]
            startPriceLabel.text = auction.start_price?.humanPrice
            currentPriceLabel.text = auction.current_price?.humanPrice
            statusLabel.text = auction.status
            typeLabel.text = auction.type
            endTimeLabel.text = auction.end_time?.dateFromISO
    }

}
