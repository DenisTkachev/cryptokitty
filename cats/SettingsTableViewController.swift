//
//  SettingsTableViewController.swift
//  cats
//
//  Created by Денис on 06.01.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

import UIKit
import CoreData

class SettingsTableViewController: UITableViewController {
    
    let row : [String] = [NSLocalizedString("HOWMANYCATS", comment: "")]
    let pickerElements = ["5","10","20","40","80"]
    
    var settings: Settings?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("SETTINGS", comment: "")
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        settings = CoreDataHandler.fetchObject()
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return row.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        cell.selectionStyle = .none
        cell.textLabel?.text = row[indexPath.row]
        cell.textLabel?.font.withSize(12)
        cell.textLabel?.font = UIFont.systemFont(ofSize: 13.0)
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.0)
        cell.detailTextLabel?.text = settings?.countOfCats ?? "5"
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let picker = PickerViewDialog(textColor: .black, buttonColor: .black, font: .boldSystemFont(ofSize: 12.0), locale: nil, showCancelButton: true)
        picker.show(row[indexPath.row], data: pickerElements) { (selectedValue) in
            let cell = tableView.cellForRow(at: indexPath)
            if selectedValue != nil {
                cell?.detailTextLabel?.text = selectedValue
            }
            CoreDataHandler.saveSetting(value: (selectedValue) ?? "5")
        }
    }
    
}

