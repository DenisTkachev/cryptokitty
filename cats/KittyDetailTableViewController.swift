//
//  KittyDetailTableViewController.swift
//  cats
//
//  Created by Денис on 06.01.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

import UIKit

class KittyDetailTableViewController: UITableViewController {
    
    var auction = Auctions()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("DETAILS", comment: "")
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("OPENLINK", comment: ""), style: .plain, target: self, action: #selector(goToLink))
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // Auctions, Seller, Kittie
        return 3
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 7
        case 1:
            return 2
        case 2:
            return 12
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("AUCTION", comment: "")
        case 1:
            return NSLocalizedString("SELLER", comment: "")
        case 2:
            return NSLocalizedString("KITTIE", comment: "")
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        switch indexPath.section {
        case 0: // Auction
            if indexPath == [0,0] {
                cell.textLabel?.text = NSLocalizedString("CURRENTPRICE", comment: "")
                cell.detailTextLabel?.text = auction.start_price?.humanPrice
            }
            if indexPath == [0,1] {
                cell.textLabel?.text = NSLocalizedString("ENDPRICE",comment:"")
                cell.detailTextLabel?.text = auction.end_price?.humanPrice
            }
            if indexPath == [0,3]{
                cell.textLabel?.text = NSLocalizedString("STARTTIME",comment:"")
                cell.detailTextLabel?.text = auction.start_time?.dateFromISO
            }
            if indexPath == [0,4] {
                cell.textLabel?.text = NSLocalizedString("ENDTIME",comment:"")
                cell.detailTextLabel?.text = auction.end_time?.dateFromISO
            }
            if indexPath == [0,5] {
                cell.textLabel?.text = NSLocalizedString("CURRENTPRICE",comment:"")
                cell.detailTextLabel?.text = auction.current_price?.humanPrice
            }
            if indexPath == [0,6] {
                cell.textLabel?.text = NSLocalizedString("SELLERNAME",comment:"")
                cell.detailTextLabel?.text = auction.seller?.nickname
            }
            if indexPath == [0,7] {
                cell.textLabel?.text = NSLocalizedString("KITTIENAME",comment:"")
                cell.detailTextLabel?.text = auction.kitty?.name
            }
            return cell
        case 1: // Seller
            if indexPath == [1,0] {
                cell.textLabel?.text = NSLocalizedString("NICKNAME",comment:"")
                cell.detailTextLabel?.text = auction.seller?.nickname
            }
            if indexPath == [1,1] {
                cell.textLabel?.text = NSLocalizedString("ADDRESS",comment:"")
                cell.detailTextLabel?.adjustsFontSizeToFitWidth = true
                cell.detailTextLabel?.text = auction.seller?.address
            }
            
            return cell
        case 2: // Kittie
            if let kitty = auction.kitty {
                if indexPath == [2,0] {
                    cell.textLabel?.text = NSLocalizedString("ID",comment:"")
                    cell.detailTextLabel?.text = String(describing: kitty.id!)
                }
                if indexPath == [2,1] {
                    cell.textLabel?.text = NSLocalizedString("COLOR",comment:"")
                    cell.detailTextLabel?.text = kitty.color
                }
                if indexPath == [2,2] {
                    cell.textLabel?.text = NSLocalizedString("CREATEDDATE",comment:"")
                    cell.detailTextLabel?.text = kitty.created_at
                }
                if indexPath == [2,3] {
                    cell.textLabel?.text = NSLocalizedString("FANCYTYPE",comment:"")
                    cell.detailTextLabel?.text = kitty.fancy_type
                }
                if indexPath == [2,4] {
                    cell.textLabel?.text = NSLocalizedString("GENERATION",comment:"")
                    cell.detailTextLabel?.text = String(describing: kitty.generation!)
                }
                if indexPath == [2,5] {
                    cell.textLabel?.text = NSLocalizedString("EXCLUSIVE",comment:"")
                    cell.detailTextLabel?.text = String(describing: kitty.is_exclusive!)
                }
                if indexPath == [2,6] {
                    cell.textLabel?.text = NSLocalizedString("NAME",comment:"")
                    cell.detailTextLabel?.text = kitty.name
                }
                if indexPath == [2,7] {
                    cell.textLabel?.text = NSLocalizedString("OWNER",comment:"")
                    cell.detailTextLabel?.text = kitty.owner?.nickname
                }
                if indexPath == [2,8] {
                    cell.textLabel?.text = NSLocalizedString("COOLDOWN",comment:"")
                    cell.detailTextLabel?.text = String(kitty.status!.cooldown!.dateFromTimeStamp)
                }
                if indexPath == [2,9] {
                    cell.textLabel?.text = NSLocalizedString("COOLDOWNINDEX",comment:"")
                    let index = kitty.status!.cooldown_index!
                    cell.detailTextLabel?.text = Array(coolDownSetGlobal)[index].key
                }
                if indexPath == [2,10] {
                    cell.textLabel?.text = NSLocalizedString("GESTATING",comment:"")
                    cell.detailTextLabel?.text = String(describing: kitty.status!.is_gestating!)
                }
                if indexPath == [2,11] {
                    cell.textLabel?.text = NSLocalizedString("READY",comment:"")
                    cell.detailTextLabel?.text = String(describing: kitty.status!.is_ready!)
                }
            }
            
            return cell
        default:
            return cell
        }
    }
    // open in browser
    @objc func goToLink() {
        if let id = auction.kitty?.id, let url = NSURL(string: "https://www.cryptokitties.co/kitty/\(id)") {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
}





