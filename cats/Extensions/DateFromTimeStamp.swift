//
//  DateFromTimeStamp.swift
//  cats
//
//  Created by Денис on 18.02.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

import Foundation

public extension Int {
    
    var dateFromTimeStamp: String {
        //let temp = self.dropLast(3)
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM-dd HH:mm"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
}
