
import Foundation
import UIKit
import CoreData


extension ViewController {
    
    func loadImages(_ fetched:@escaping (_ images:[Thumbnail]) -> Void) {
        //startActivity()
        Run.async(coreDataQueue) {
            guard let moc = self.managedContext else {
                return
            }
            let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateMOC.parent = moc
            privateMOC.perform {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Const.CoreData.Thumbnail)
                do {
                    let results = try privateMOC.fetch(fetchRequest)
                    let maybeImageData = results as? [Thumbnail]
                    
                    guard let imageData = maybeImageData else {
                        Run.main {
                            //self.noImagesFound()
                        }
                        return
                    }
                    //self.stopActivity()
//                    Run.main {
                        fetched(imageData.filter { thumbnail in
                            return thumbnail.imageData != nil && thumbnail.id != nil
                       //     return true
                        })
//                    }
                } catch {
                    //self.stopActivity()
                    Run.main {
                        //self.noImagesFound()
                    }
                    return
                }
            }
        }
    }
}
