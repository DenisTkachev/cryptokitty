//
//  String+IndexArray.swift
//  cats
//
//  Created by Денис on 18.03.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

import Foundation
extension String {
    func indexArray(of character: Character) -> Int? {
        guard let index = index(of: character) else { return nil }
        return distance(from: startIndex, to: index)
    }
}
