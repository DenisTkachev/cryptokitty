//
//  String+humanPrice.swift
//  cats
//
//  Created by Денис on 06.01.2018.
//  Copyright © 2018 Denis. All rights reserved.
//

//import Foundation

public extension String {
    var humanPrice: String {
        let temp = String((Double(self)! / 1000000000000000000))
        let first5 = String(temp.prefix(7))
        return String(first5) }
}
