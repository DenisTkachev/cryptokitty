//
//  ViewController.swift
//  cats
//
//  Created by Денис on 21.12.2017.
//  Copyright © 2017 Denis. All rights reserved.
//

import UIKit
import WebKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    func saveToCache(id: Double, image: UIImage) {        
        prepareImageForSaving(image: image, id: id)
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var wellcomeScreenView: UIView!
    @IBOutlet weak var settingsBtn: UIBarButtonItem!
    @IBOutlet weak var filterBtn: UIBarButtonItem!
    
    var inSearchMode = false
    let viewLoader = UIView()
    var dataAllAuctions: AllAuctions?
    var savedImage = [Int : UIImage]()
    var filteredDataAllAuctions = AllAuctions()
    var currentOffset: UInt8 = 0
    
    let imageProcessingQueue = DispatchQueue(label: "imageProcessingQueue", attributes: DispatchQueue.Attributes.concurrent)
    let coreDataQueue = DispatchQueue(label: "coreDataQueue")
    var managedContext : NSManagedObjectContext?
    var settings: Settings?
    let pawView = UIImageView(image: UIImage(named: "paw"))
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.addSubview(self.refreshControl)
        showWellComeScreen()
        coreDataSetup()
        fetchImage() // fetch from CoreData
        setupView()
        showLoaderView(true)
        setupSwipe()
        
        if InternetStatus.isInternetAvailable() {
            loadMainData(offset: currentOffset)
        } else {
            showAlertNoInternet()
        }

        
        
        tableView.register(UINib(nibName: "AuctionTableViewCell", bundle: nil), forCellReuseIdentifier: "AuctionTableViewCell")
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.placeholder = NSLocalizedString("SEARCHBARPLACEHOLDER", comment: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
            settings = CoreDataHandler.fetchObject()
    }
    
    @IBAction func pressSettings(_ sender: UIBarButtonItem) {
        self.hideKeyboardWhenTappedAround()
        let settingsVC = self.storyboard!.instantiateViewController(withIdentifier: "SettingsTableViewController") as! SettingsTableViewController
        navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    @IBAction func closeWellcomeScreen(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "firstRun")
        wellcomeScreenView.isHidden = true
        wellcomeScreenView.removeFromSuperview()
        settingsBtn.isEnabled = true
        filterBtn.isEnabled = true
    }
    @IBAction func pressFilter(_ sender: UIBarButtonItem) {
        self.hideKeyboardWhenTappedAround()
        let filterVC = self.storyboard!.instantiateViewController(withIdentifier: "FilterTableViewController") as! FilterTableViewController
        navigationController?.pushViewController(filterVC, animated: true)
    }
    
    func showWellComeScreen() {
        if UserDefaults.standard.bool(forKey: "firstRun") {
            wellcomeScreenView.isHidden = true
            print("NOTfirstRun")
        } else {
            wellcomeScreenView.isHidden = false
            filterBtn.isEnabled = false
            settingsBtn.isEnabled = false
            print("firstRun")
        }
    }
    
    // MARK: TableViewSetup
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode {
            return filteredDataAllAuctions.auctions?.count ?? 0
        } else {
            return dataAllAuctions?.auctions?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AuctionTableViewCell", for: indexPath) as! AuctionTableViewCell
        
        cell.spinner.startAnimating()
        var sourceData: [Auctions]?
        
        if inSearchMode {
            sourceData = filteredDataAllAuctions.auctions
        } else {
            sourceData = dataAllAuctions?.auctions
        }
        
        if let auctions = sourceData {
            
            cell.configure(auctions, indexPath: indexPath)
            if let id = Int(cell.idLabel.text!), savedImage.keys.contains(id) {
                print(id, "<- from CoreData")
                cell.imageCat.image = savedImage[id]
                cell.spinner.stopAnimating()
            } else { // download when not in cache
                cell.spinner.startAnimating()
                print("Download image")
                if let kitty = auctions[indexPath.row].kitty {
                    let url = kitty.image_url ?? ""
                    // convert image from svg to png
                    let urlGet = "http://denistkachev.ru/php.php?url=\(url)"
                    ImageCacheLoader().obtainImageWithPath(imagePath: urlGet) { (image) in
                        if let updateCell = tableView.cellForRow(at: indexPath) as? AuctionTableViewCell {
                            let id = Double(updateCell.idLabel.text!)!
                            self.saveToCache(id: id, image: image)
                            updateCell.selectionStyle = .none
                            updateCell.imageCat.image = image
                            updateCell.spinner.stopAnimating()
                        }
                    }
                fetchImage()
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.hideKeyboardWhenTappedAround()
        let kittyDetail = self.storyboard!.instantiateViewController(withIdentifier: "KittyDetailTableViewController") as! KittyDetailTableViewController
        
        if let auctions = dataAllAuctions?.auctions {
            kittyDetail.auction = auctions[indexPath.row]
            navigationController?.pushViewController(kittyDetail, animated: true)
        }
    }
}

extension ViewController {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false
            view.endEditing(true)
            tableView.reloadData()
        } else {
            inSearchMode = true
            guard let dataAll = dataAllAuctions else { return }
            guard let data = dataAll.auctions else { return }
            
            filteredDataAllAuctions.auctions = (data.filter({
                let kittie = $0.kitty!.id!
                return String(kittie).contains(searchBar.text!)
            }))
            tableView.reloadData()
        }
    }
    
    func reloadTableView() {
        DispatchQueue.main.sync {
            self.tableView.reloadData()
            showLoaderView(false)
            showFootPrint()
        }
    }
    
    func setupView() {
        title = "Kitties Monitor"
        viewLoader.backgroundColor = .white
        viewLoader.frame = UIScreen.main.bounds
        pawView.center = view.center
        view.addSubview(viewLoader)
        view.addSubview(pawView)
    }
    
    func showLoaderView(_ state: Bool) {
        switch state {
        case true:
            viewLoader.isHidden = false
            pawView.isHidden = false
            tableView.bounces = false
            DispatchQueue.main.async {
                self.tabBarController?.tabBar.isHidden = true
            }
        case false:
            DispatchQueue.main.async {
                self.viewLoader.isHidden = true
                self.pawView.isHidden = true
                self.pawView.layer.removeAllAnimations()
                self.tableView.bounces = true
                self.tabBarController?.tabBar.isHidden = false
                self.tableView.reloadData()
            }
        }
    }
    
    func loadMainData(offset: UInt8) {
        // send request to server
        var limit = 5
        if let countOfCats = CoreDataHandler.fetchObject() {
            limit = Int(countOfCats.countOfCats) ?? 5
        }
        showLoaderView(true)
        showFootPrint()
        NetworkApiService.getAllAuctions(type: .sale, status: .open, limit: limit, offset: offset) { (responseObject, error) in
            if let responseObject = responseObject {
                self.dataAllAuctions = responseObject
                // apply main filters!
                self.dataAllAuctions?.auctions = self.dataAllAuctions?.auctionsFiltered
                
                self.reloadTableView()
                self.showLoaderView(false)
            }
            return
        }
    }
    
    func showFootPrint() {
        let animation = CABasicAnimation(keyPath: "opacity")
        animation.autoreverses = true
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = 1
        animation.repeatCount = .infinity
        animation.isRemovedOnCompletion = false
        pawView.layer.add(animation, forKey: nil)
        pawView.startAnimating()
    }
    
    func setupSwipe() {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if UserDefaults.standard.bool(forKey: "firstRun") {
            if gesture.direction == UISwipeGestureRecognizerDirection.right {
                print("Swipe Right")
                if let catsPerPage = settings?.countOfCats, let catsPerPageCount = UInt8(catsPerPage) {
                    currentOffset = currentOffset + catsPerPageCount
                    loadMainData(offset: currentOffset)
                }
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.left {
                print("Swipe Left")
                if let catsPerPage = settings?.countOfCats, let catsPerPageCount = UInt8(catsPerPage) {
                    currentOffset = catsPerPageCount - currentOffset
                    loadMainData(offset: currentOffset)
                }
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.up {
                print("Swipe Up")
            }
            else if gesture.direction == UISwipeGestureRecognizerDirection.down {
                print("Swipe Down")
            }
        }
    }
}

extension ViewController {
    func showAlertNoInternet() {
        let alert = UIAlertController(title: NSLocalizedString("NOINTERNET", comment: ""), message: NSLocalizedString("NOINTERNETMESSAGE", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (action) -> Void in
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func fetchImage() {
        // loadImage function with a completion block
        savedImage.removeAll()
        loadImages { (thumbnails) -> Void in
            thumbnails.forEach {
                self.savedImage[$0.id as! Int] = $0.image
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        loadMainData(offset: currentOffset)
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
}
